import React, { Component } from "react";
import { Link as LinkTo } from "react-router-dom";
import {
  MDBCardText, MDBCard, MDBCardBody, MDBCardTitle,
  MDBRow,
  MDBCol,
  MDBCarousel,
  MDBCarouselCaption,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBView,
  MDBMask,
  MDBBtn
} from "mdbreact";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faChevronCircleRight } from '@fortawesome/free-solid-svg-icons'

import "../../assets/css/NewsPage.css";

class NewsPage extends Component {

  linkTo(route) {
    this.props.history.push(route);
  }

  render() {
    const loop = [1, 2, 3];
    const string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a...";

    return (
      <>
        <MDBView>
          <div className="row">
            <div className="col-lg-8 col-sm-12 padding-null">
              <MDBCarousel
                activeItem={1}
                length={3}
                showControls={true}
                showIndicators={true}
                className="card-img-top"
              >
                <MDBCarouselInner>
                  <MDBCarouselItem itemId="1">
                    <MDBView>
                      <img
                        className="d-block w-100"
                        src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
                        alt="First slide"
                      />
                      <MDBMask overlay="black-light" />
                    </MDBView>
                    <MDBCarouselCaption>
                      <h3 className="h3-responsive">Light mask</h3>
                      <p>First text</p>
                    </MDBCarouselCaption>
                  </MDBCarouselItem>
                  <MDBCarouselItem itemId="2">
                    <MDBView>
                      <img
                        className="d-block w-100"
                        src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg"
                        alt="Second slide"
                      />
                      <MDBMask overlay="black-strong" />
                    </MDBView>
                    <MDBCarouselCaption>
                      <h3 className="h3-responsive">Strong mask</h3>
                      <p>Second text</p>
                    </MDBCarouselCaption>
                  </MDBCarouselItem>
                  <MDBCarouselItem itemId="3">
                    <MDBView>
                      <img
                        className="d-block w-100"
                        src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg"
                        alt="Third slide"
                      />
                      <MDBMask overlay="black-slight" />
                    </MDBView>
                    <MDBCarouselCaption>
                      <h3 className="h3-responsive">Slight Mast</h3>
                      <p>Third text</p>
                    </MDBCarouselCaption>
                  </MDBCarouselItem>
                </MDBCarouselInner>
              </MDBCarousel>
            </div>
            <div className="col-lg-4 col-sm-12 padding-null">
              <div className="news-jargon">
                <p className="news-jargon-text">Creative<br></br>Innovative Collaborative</p>
                <p className="news-jargon-text-tikom">TIKomDik</p>
                <div className="font-jargon-empowers-header padding-null">
                  <p>Empowers To Educate</p>
                </div>
              </div>
            </div>
          </div>
          <MDBCol
            size="12"
            className="NewsCol"
            style={{
              backgroundImage:
                "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)",
            }}
          >
            <div className="d-flex justify-content-center align-items-center row" style={{ height: '100%' }}>
              <div className="overlay-header-news" style={{ backgroundColor: '#164d9f' }}></div>
              <div className="input-group contents-header-news">
                <div className="col-sm-12 col-lg-4 d-flex justify-content-center align-items-center flex-row" style={{ margin: "auto", color: "snow" }}>
                  <div>Informasi seputar pendidikan</div><div className="tag-new-news">&nbsp;Baca Berita Baru</div>
                </div>
                <div className="col-sm-12 col-lg-8 d-flex justify-content-center align-items-center">
                  <div className="input-group row">
                    <input type="text" className="form-control" id="abc" style={{ backgroundColor: "white" }} placeholder="Pencarian Berita" />
                    <div className="input-group-append">
                      <MDBBtn
                        color="grey"
                        className="m-0 px-3 py-2 z-depth-0"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </MDBBtn>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </MDBCol>
          <MDBRow>
            <MDBCol md="12">
              <MDBView style={{ margin: '0.01%' }}>
                <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                  <div
                    className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column"
                    style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}
                    onClick={() => this.linkTo('/news/education')}
                  >
                    <div className="overlay-col-separator-yellow" />
                    <h2 className="col-separator-subtitle align-self-center">Education</h2>
                  </div>
                  <div
                    className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column"
                    style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}
                    onClick={() => this.linkTo('/news/technology')}
                  >
                    <div className="overlay-col-separator-blue" />
                    <h2 className="col-separator-subtitle align-self-center">Technology</h2>
                  </div>
                  <div
                    className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column"
                    style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}
                    onClick={() => this.linkTo('/news/system')}
                  >
                    <div className="overlay-col-separator-green" />
                    <h2 className="col-separator-subtitle align-self-center">System/Policy</h2>
                  </div>
                </div>
              </MDBView>
            </MDBCol>
            <MDBCol size="12">
              <div style={{ height: '8vh' }}></div>
            </MDBCol>
            <MDBCol lg="1" sm="12" md="12">

            </MDBCol>
            <MDBCol lg="7">
              {loop.map((x, key) => {
                return (
                  <MDBView key={key} onClick={() => this.linkTo('/news/education/detail/' + 'new-education-on-2021')}>
                    <div className="row justify-content-md-center" style={{ margin: "0.01%" }}>
                      <div className="col-12 col-lg-4 news-separator d-flex justify-content-end flex-column"
                        style={{
                          backgroundImage:
                            "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)",
                        }}
                      >
                        <div className="news-separator-overlay-yellow" />
                        <h2 className="news-separator-title p-4">
                          "Everybody Get Up and Sleep Again"
                        </h2>
                      </div>
                      <div className="col-12 col-lg-4 news-separator d-flex justify-content-end flex-column"
                        style={{
                          backgroundImage:
                            "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)",
                        }}
                      >
                        <div className="news-separator-overlay-green" />
                        <h2 className="news-separator-title p-4">
                          "Everybody Get Up and Sleep Again"
                        </h2>
                      </div>
                      <div className="col-12 col-lg-4 news-separator d-flex justify-content-end flex-column"
                        style={{
                          backgroundImage:
                            "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)",
                        }}
                      >
                        <div className="news-separator-overlay-blue" />
                        <h2 className="news-separator-title p-4">
                          "Everybody Get Up and Sleep Again"
                        </h2>
                      </div>
                    </div>
                  </MDBView>
                )
              })}
            </MDBCol>
            <MDBCol lg="3" sm="12" md="12">
              <div className="row card-side-programs">
                <div className="col-12 d-flex justify-content-start align-items-center mt-2">
                  <MDBCard>
                    <MDBCardBody>
                      <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="Pencarian Programs" aria-label="Username" aria-describedby="basic-addon" />
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="basic-addon">
                            <FontAwesomeIcon icon={faSearch} />
                          </span>
                        </div>
                      </div>
                      <MDBCardTitle>Programs</MDBCardTitle>
                      <hr />
                      <div>
                        <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                        <div className="news-title">Renja Pendidikan Jabar Harus Kembangkan Inovasi</div>
                        <div className="news-content">
                          {string.substring(0, 150) + '. . . '} <LinkTo to="#" style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                        </div>
                      </div>
                      <hr />
                      <div>
                        <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                        <div className="news-title">
                          Renja Pendidikan Jabar Harus Kembangkan Inovasi
                        </div>
                        <div className="news-content">
                          {string.substring(0, 150) + '. . . '} <LinkTo to="#" style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                        </div>
                      </div>

                    </MDBCardBody>
                  </MDBCard>
                </div>
              </div>
            </MDBCol>
            <MDBCol lg="1" sm="12" md="12">
            </MDBCol>
            <MDBCol size="12">
              <div style={{ height: '3vh', backgroundColor: '#f2f2f2', marginTop: '5vh' }}></div>
            </MDBCol>
            <MDBCol size="12">
              <div style={{ height: '5vh' }}></div>
            </MDBCol>
          </MDBRow>
        </MDBView>
      </>
    );
  }
}

export default NewsPage;
