import React, { Component } from "react";
import { Link as LinkTo } from "react-router-dom";
import {
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBCarousel,
  MDBCarouselCaption,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBView,
  MDBMask,
  MDBCardBody,
  MDBBox,
  MDBTypography,
  MDBPageNav,
  MDBPageItem,
  MDBPagination,
  MDBContainer,
  MDBBtn,
  MDBCard,
  MDBCardTitle,
} from "mdbreact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt, faEye, faSearch, faTags, faUser } from "@fortawesome/free-solid-svg-icons";


class NewsPageEducation extends Component {
  linkTo(route) {
    this.props.history.push(route);
  }
  render() {
    const loop = [1, 2, 3, 4, 5];
    const string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a...";
        
    return (
      <MDBView>
        <MDBRow>
          <MDBCol md="12">
            <div className="row">
              <div className="col-lg-8 col-sm-12 padding-null">
                <MDBCarousel
                  activeItem={1}
                  length={3}
                  showControls={true}
                  showIndicators={true}
                  className="card-img-top"
                >
                  <MDBCarouselInner>
                    <MDBCarouselItem itemId="1">
                      <MDBView>
                        <img
                          className="d-block w-100"
                          src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
                          alt="First slide"
                        />
                        <MDBMask overlay="black-light" />
                      </MDBView>
                      <MDBCarouselCaption>
                        <h3 className="h3-responsive">Light mask</h3>
                        <p>First text</p>
                      </MDBCarouselCaption>
                    </MDBCarouselItem>
                    <MDBCarouselItem itemId="2">
                      <MDBView>
                        <img
                          className="d-block w-100"
                          src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg"
                          alt="Second slide"
                        />
                        <MDBMask overlay="black-strong" />
                      </MDBView>
                      <MDBCarouselCaption>
                        <h3 className="h3-responsive">Strong mask</h3>
                        <p>Second text</p>
                      </MDBCarouselCaption>
                    </MDBCarouselItem>
                    <MDBCarouselItem itemId="3">
                      <MDBView>
                        <img
                          className="d-block w-100"
                          src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg"
                          alt="Third slide"
                        />
                        <MDBMask overlay="black-slight" />
                      </MDBView>
                      <MDBCarouselCaption>
                        <h3 className="h3-responsive">Slight Mast</h3>
                        <p>Third text</p>
                      </MDBCarouselCaption>
                    </MDBCarouselItem>
                  </MDBCarouselInner>
                </MDBCarousel>
              </div>
              <div className="col-lg-4 col-sm-12 padding-null">
                <div className="news-jargon">
                  <p className="news-jargon-text">Creative<br></br>Innovative Collaborative</p>
                  <p className="news-jargon-text-tikom">TIKomDik</p>
                  <div className="font-jargon-empowers-header padding-null">
                    <p>Empowers To Educate</p>
                  </div>
                </div>
              </div>
            </div>
          </MDBCol>
          <MDBCol
            size="12"
            className="NewsCol"
            style={{
              backgroundImage:
                "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)",
            }}
          >
            <div className="d-flex justify-content-center align-items-center row" style={{ height: '100%' }}>
              <div className="overlay-header-news" style={{ backgroundColor: '#164d9f' }}></div>
              <div className="input-group contents-header-news">
                <div className="col-sm-12 col-lg-4 d-flex justify-content-center align-items-center flex-row" style={{ margin: "auto", color: "snow" }}>
                  <div>Informasi seputar pendidikan</div><div className="tag-new-news">&nbsp;Baca Berita Edukasi</div>
                </div>
                <div className="col-sm-12 col-lg-8 d-flex justify-content-center align-items-center">
                  <div className="input-group row">
                    <input type="text" className="form-control" id="abc" style={{ backgroundColor: "white" }} placeholder="Pencarian Berita" />
                    <div className="input-group-append">
                      <MDBBtn
                        color="grey"
                        className="m-0 px-3 py-2 z-depth-0"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </MDBBtn>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </MDBCol>
        </MDBRow>

        <MDBRow size style={{ marginTop: '5vh' }}>
          <MDBCol lg="2"></MDBCol>
          <MDBCol lg="8">
            <MDBRow className="justify-content-md-center">
              <MDBCol lg="8">
                {loop.map((x, key) => {
                  return (
                    <>
                      <MDBView display="flex" justifyContent="center">
                        <MDBRow>
                          <MDBCol lg="6">
                            <MDBView className="rounded z-depth-2" hover waves onClick={() => this.linkTo("/news/education/detail/" + "education-news-one")}>
                              <img

                                className="img-fluid"
                                src="https://mdbootstrap.com/img/Photos/Others/img%20(27).jpg"
                                alt=""
                              />
                              <a href="/NewsEducation">
                                <MDBMask overlay="white-slight" />
                              </a>
                            </MDBView>
                          </MDBCol>
                          <MDBCol lg="6">
                            <MDBCol size="12">
                              <div className="mt-5">
                                <p className="news-title">
                                  Nam libero tempore, cum  soluta nobis est eligendi optio
                                  cumque nihil impedit quo minus id quod maxime placeat
                                  facere
                                </p>
                              </div>
                            </MDBCol>
                            <MDBCol size="12" style={{ display: 'flex', alignItems: 'flex-end', height: '40%', backgroundColor: '' }}>
                              <div className="row subtitle-detail-content">
                                <div className="col-lg-auto col-md-auto col-sm-6">
                                  <span><FontAwesomeIcon icon={faUser} /></span> &nbsp;Oleh Tikompedia
                                </div>
                                <div className="col-lg-auto col-md-auto col-sm-6">
                                  <span><FontAwesomeIcon icon={faCalendarAlt} /></span> &nbsp;24 Maret, 14:30 WIB
                                </div>
                                <div className="col-lg-auto col-md-auto col-sm-6">
                                  <span><FontAwesomeIcon icon={faEye} /></span> &nbsp;90 Views
                                </div>
                                <div className="col-lg-auto col-md-auto col-sm-6">
                                  <span><FontAwesomeIcon icon={faTags} flip="horizontal" /></span> &nbsp;Education, Technology
                                </div>
                              </div>
                            </MDBCol>
                          </MDBCol>
                        </MDBRow>
                      </MDBView>
                      <hr />
                    </>
                  )
                })}
                <MDBRow>
                  <MDBCol className="d-flex justify-content-center">
                    <MDBPagination className="mb-5">
                      <MDBPageItem>
                        <MDBPageNav aria-label="Previous">
                          <span aria-hidden="true">Previous</span>
                        </MDBPageNav>
                      </MDBPageItem>
                      <MDBPageItem>
                        <MDBPageNav>1</MDBPageNav>
                      </MDBPageItem>
                      <MDBPageItem>
                        <MDBPageNav>2</MDBPageNav>
                      </MDBPageItem>
                      <MDBPageItem>
                        <MDBPageNav>3</MDBPageNav>
                      </MDBPageItem>
                      <MDBPageItem>
                        <MDBPageNav aria-label="Previous">
                          <span aria-hidden="true">Next</span>
                        </MDBPageNav>
                      </MDBPageItem>
                    </MDBPagination>
                  </MDBCol>
                </MDBRow>
              </MDBCol>
              <MDBCol lg="4">
                <MDBCard>
                  <MDBCardBody>
                    <div className="input-group mb-3">
                      <input type="text" className="form-control" placeholder="Pencarian Berita" aria-label="Username" aria-describedby="basic-addon" />
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon">
                          <FontAwesomeIcon icon={faSearch} />
                        </span>
                      </div>
                    </div>
                    <MDBCardTitle>Berita Terbaru</MDBCardTitle>
                    <hr />
                    <div>
                      <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                      <div className="news-title">
                        Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                      <div className="news-content">
                        {string.substring(0, 150) + '. . . '} <LinkTo to={ '/news/education/detail/' + 'news-education-side-one' } style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                      </div>
                    </div>
                    <hr />
                    <div>
                      <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                      <div className="news-title">
                        Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                      <div className="news-content">
                        {string.substring(0, 150) + '. . . '} <LinkTo to={ '/news/technology/detail/' + 'news-education-side-two' } style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                      </div>
                    </div>
                    <hr />
                    <div>
                      <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                      <div className="news-title">
                        Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                      <div className="news-content">
                        {string.substring(0, 150) + '. . . '} <LinkTo to={ '/news/system/detail/' + 'news-education-side-one' } style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                      </div>

                    </div>
                    <div>
                      <div className="news-all">
                        <LinkTo to={ '/news' } style={{ color: '#0057e7' }}>Lihat Semua Berita <MDBIcon icon="angle-double-right" /></LinkTo>
                      </div>
                    </div>
                  </MDBCardBody>
                </MDBCard>
              </MDBCol>
            </MDBRow>
          </MDBCol>
          <MDBCol lg="2"></MDBCol>
        </MDBRow>

        <MDBView>
          <div className="row title-upcoming" style={{ marginTop: '15vh' }}>
            <div className="col-lg-1 col-sm-12" style={{ backgroundColor: '#fec71c' }} />
            <div className="col-lg-2 col-sm-12 sub-title">
              Lates News
                        </div>
            <div className="col-lg-8 col-sm-12 d-flex align-items-center">
              <button className="btn btn-outline-warning" style={{ fontSize: '1rem' }}>Selengkapnya</button>
            </div>
          </div>
          <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
            <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
              <div className="overlay-col-separator-yellow" />
              <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
            </div>
            <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
              <div className="overlay-col-separator-blue" />
              <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
            </div>
            <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
              <div className="overlay-col-separator-green" />
              <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
            </div>
          </div>
        </MDBView>
      </MDBView>
    );
  }
}

export default NewsPageEducation;
