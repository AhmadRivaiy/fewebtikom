import React, { Component } from "react";
import { Link as LinkTo } from "react-router-dom";
import {
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBCarousel,
  MDBCarouselCaption,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBView,
  MDBMask,
  MDBCardBody,
  MDBContainer,
  MDBTypography,
  MDBCard,
  MDBCardTitle,
} from "mdbreact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt, faEye, faSearch, faTags, faUser } from "@fortawesome/free-solid-svg-icons";


class NewsPageEducation extends Component {
  linkTo(route) {
    this.props.history.push(route);
  }
  render() {
    const string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a...";

    return (
      <MDBView>
        <MDBView>
          <MDBCarousel
            activeItem={1}
            length={3}
            showControls={true}
            showIndicators={true}
            className="card-img-top"
          >
            <MDBCarouselInner>
              <MDBCarouselItem itemId="1">
                <MDBView>
                  <img
                    className="d-block w-100"
                    src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
                    alt="First slide"
                  />
                  <MDBMask overlay="black-light" />
                </MDBView>
                <MDBCarouselCaption>
                  <h3 className="h3-responsive">Light mask</h3>
                  <p>First text</p>
                </MDBCarouselCaption>
              </MDBCarouselItem>
              <MDBCarouselItem itemId="2">
                <MDBView>
                  <img
                    className="d-block w-100"
                    src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg"
                    alt="Second slide"
                  />
                  <MDBMask overlay="black-strong" />
                </MDBView>
                <MDBCarouselCaption>
                  <h3 className="h3-responsive">Strong mask</h3>
                  <p>Second text</p>
                </MDBCarouselCaption>
              </MDBCarouselItem>
              <MDBCarouselItem itemId="3">
                <MDBView>
                  <img
                    className="d-block w-100"
                    src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg"
                    alt="Third slide"
                  />
                  <MDBMask overlay="black-slight" />
                </MDBView>
                <MDBCarouselCaption>
                  <h3 className="h3-responsive">Slight Mast</h3>
                  <p>Third text</p>
                </MDBCarouselCaption>
              </MDBCarouselItem>
            </MDBCarouselInner>
          </MDBCarousel>
        </MDBView>
        <MDBRow
          className="justify-content-md-center margin-side"
          style={{ marginTop: '5vh' }}
        >
          <MDBCol lg="2"></MDBCol>
          <MDBCol lg="5">
            <MDBTypography className="title-detail-content" tag="h5" style={{ color: "#3385ff", fontSize: "30px" }}>
              Kembangkan Pendidikan Vokasi, Disdik Jabar Gandeng Polman Bandung
            </MDBTypography>

            <div className="row subtitle-detail-content">
              <div className="col-lg-auto col-md-auto col-sm-6">
                <span><FontAwesomeIcon icon={faUser} /></span> &nbsp;Oleh Tikompedia
              </div>
              <div className="col-lg-auto col-md-auto col-sm-6">
                <span><FontAwesomeIcon icon={faCalendarAlt} /></span> &nbsp;24 Maret, 14:30 WIB
              </div>
              <div className="col-lg-auto col-md-auto col-sm-6">
                <span><FontAwesomeIcon icon={faEye} /></span> &nbsp;90 Views
              </div>
              <div className="col-lg-auto col-md-auto col-sm-6">
                <span><FontAwesomeIcon icon={faTags} flip="horizontal" /></span> &nbsp;Education, Technology
              </div>
            </div>

            <MDBTypography
              style={{ letterSpacing: "2px", textAlign: "justify", marginTop: '5vh' }}
            >
              <p>
                Bandung, Disdik Jabar - Dinas Pendidikan (Disdik) Jawa Barat
                (Jabar) resmi bekerja sama dengan Politeknik Manufakturing
                (Polman) Bandung untuk pengembangan pendidikan sekolah menengah
                kejuruan (SMK) di Jabar.
              </p>
              <p>
                Salah satu bentuk nyata kerja sama tersebut adalah dibangunnya
                Polman Bandung di Majalengka yang terintegrasi dengan SMK
                melalui program SMK D2-Fast Track.
              </p>
              <p>
                Nantinya, lanjut Kadisdik, akan dibuat tiga kompetensi keahlian
                baru di SMK yang menyesuaikan dengan kebutuhan industri di
                daerah tersebut, salah satunya kriya logam. Pengembangan
                kompetensi keahlian tersebut akan dibina langsung oleh Polman
                Bandung di Majalengka. "Setelah ini akan berjalan pelaksanaan
                kerja sama antara politeknik, SMK, dan industri. Nanti akan
                muncul SMK D-2 Fast Track di daerah Majalengka, Cirebon, dan
                sekitar Segitiga Rebana," tutur Kadisdik Jabar, Dedi Supandi
                saat menghadiri Dies Natalies Polman Bandung ke-44 sekaligus
                penandatanganan kerja sama di Lapangan Rekawanawaluya Polman
                Bandung, Jln. Kanayakan No. 21, Kota Bandung, Kamis (25/3/2021)
              </p>
              <p>
                Kadisdik menjelaskan, sesuai amanat Gubenur Jabar, kerja sama
                ini merupakan upaya peningkatan sumber daya manusia (SDM),
                khususnya bagi pelajar. Sehingga, 25 tahun ke depan saat wilayah
                di sana sudah menjadi kota metropolitan, mereka telah siap
                berperan membangun negeri.
              </p>
              <p>
                Acara ini juga dihadiri Gubenur Jabar, Ridwan Kamil dan Direktur
                Jenderal (Dirjen) Pendidikan Sekolah Vokasi Kementerian
                Pendidikan dan Kebudayaan (Kemendikbud) RI, Wikan Sakarinto.
              </p>
              <p>
                Selain kerja sama Disdik Jabar dan Polman Bandung, ada dua
                penandatanganan kerja sama Kemendikbud dengan Pemprov Jabar dan
                Kabupaten Majalengka dalam sinergi perencanaan pembangunan
                kampus dua Polman Bandung di Majalengka serta Pemprov Jabar dan
                Polman Bandung dalam bidang penelitian, pengembangan, diseminasi
                serta penerapan hasil-hasil penelitian.***
              </p>
            </MDBTypography>
          </MDBCol>
          <MDBCol lg="3">
            <MDBCard>
              <MDBCardBody>
                <div className="input-group mb-3">
                  <input type="text" className="form-control" placeholder="Pencarian Berita" aria-label="Username" aria-describedby="basic-addon" />
                  <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon">
                      <FontAwesomeIcon icon={faSearch} />
                    </span>
                  </div>
                </div>
                <MDBCardTitle>Berita Terbaru</MDBCardTitle>
                <hr />
                <div>
                  <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                  <div className="news-title">
                    Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                  <div className="news-content">
                    {string.substring(0, 150) + '. . . '} <LinkTo to={'/news/education/detail/' + 'news-education-side-one'} style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                  </div>
                </div>
                <hr />
                <div>
                  <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                  <div className="news-title">
                    Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                  <div className="news-content">
                    {string.substring(0, 150) + '. . . '} <LinkTo to={'/news/technology/detail/' + 'news-education-side-two'} style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                  </div>
                </div>
                <hr />
                <div>
                  <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                  <div className="news-title">
                    Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                  <div className="news-content">
                    {string.substring(0, 150) + '. . . '} <LinkTo to={'/news/system/detail/' + 'news-education-side-one'} style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                  </div>

                </div>
                <div>
                  <div className="news-all">
                    <LinkTo to={'/news'} style={{ color: '#0057e7' }}>Lihat Semua Berita <MDBIcon icon="angle-double-right" /></LinkTo>
                  </div>
                </div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="2"></MDBCol>
        </MDBRow>
        <MDBView>
          <div className="row title-upcoming" style={{ marginTop: '15vh' }}>
            <div className="col-lg-1 col-sm-12" style={{ backgroundColor: '#fec71c' }} />
            <div className="col-lg-2 col-sm-12 sub-title">
              Lates News
                        </div>
            <div className="col-lg-8 col-sm-12 d-flex align-items-center">
              <button className="btn btn-outline-warning" style={{ fontSize: '1rem' }}>Selengkapnya</button>
            </div>
          </div>
          <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
            <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
              <div className="overlay-col-separator-yellow" />
              <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
            </div>
            <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
              <div className="overlay-col-separator-blue" />
              <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
            </div>
            <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
              <div className="overlay-col-separator-green" />
              <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
            </div>
          </div>
        </MDBView>
      </MDBView>
    );
  }
}

export default NewsPageEducation;
