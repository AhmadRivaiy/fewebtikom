import React from 'react';
import {
    MDBContainer,
    MDBAnimation, MDBCarousel, MDBCarouselInner, MDBCarouselItem, MDBView, MDBInputGroup, MDBBtn, MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBPagination, MDBPageItem, MDBPageNav
} from 'mdbreact';
import { Link as LinkTo } from 'react-router-dom';
import '../../assets/css/EventsPage.css';
import { Link, Element, Events } from 'react-scroll';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt, faChevronCircleRight, faClock, faEye, faFire, faSearch, faTags } from '@fortawesome/free-solid-svg-icons';
import Countdown, { zeroPad } from 'react-countdown';
import { Card, CardContent, CardMedia } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

class MainEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data_about: [],
            is_lang: [!localStorage.getItem('is_lang_now') ? 0 : JSON.parse(localStorage.getItem('is_lang_now')).param]
        };
    }
    scrollToTop = () => window.scrollTo(0, 0);

    async componentDidMount() {
        Events.scrollEvent.register('begin');
        Events.scrollEvent.register('end');
    }

    componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
    }

    beres() {
        this.setState({ isClosed: true, isOnline: false });
    }

    render() {
        const { t } = this.props;
        const string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a...";
        const onClosed = () => <h6 style={{ fontWeight: '700', color: '#d62d20' }}>Closed</h6>;

        const renderer = ({ days, hours, minutes, completed }) => {
            if (completed) {
                return (
                    <>
                        <div className="col-12 d-flex flex-row" style={{ justifyContent: 'space-around', color: '#d62d20' }}>
                            <div className="d-flex justify-content-center flex-column">
                                <div className="events-countdown" style={{  color: '#d62d20' }}>
                                    {zeroPad(days)}
                                </div>
                                <div>
                                    Day
                                </div>
                            </div>
                            <div className="events-countdown d-flex justify-content-center" style={{  color: '#d62d20' }}>
                                :
                            </div>
                            <div className="d-flex justify-content-center flex-column">
                                <div className="events-countdown d-flex justify-content-center"  style={{  color: '#d62d20' }}>
                                    {zeroPad(hours)}
                                </div>
                                <div>
                                    Hours
                                </div>
                            </div>
                            <div className="events-countdown d-flex justify-content-center" style={{  color: '#d62d20' }}>
                                :
                            </div>
                            <div className="d-flex justify-content-center flex-column">
                                <div className="events-countdown"  style={{  color: '#d62d20' }}>
                                    {zeroPad(minutes)}
                                </div>
                                <div>
                                    Min
                                </div>
                            </div>
                        </div>
                    </>
                );
            } else {
                return (
                    <>
                        <div className="col-12 d-flex flex-row" style={{ justifyContent: 'space-around' }}>
                            <div className="d-flex justify-content-center flex-column">
                                <div className="events-countdown">
                                    {zeroPad(days)}
                                </div>
                                <div>
                                    Day
                                </div>
                            </div>
                            <div className="events-countdown d-flex justify-content-center">
                                :
                            </div>
                            <div className="d-flex justify-content-center flex-column">
                                <div className="events-countdown d-flex justify-content-center">
                                    {zeroPad(hours)}
                                </div>
                                <div>
                                    Hours
                                </div>
                            </div>
                            <div className="events-countdown d-flex justify-content-center">
                                :
                            </div>
                            <div className="d-flex justify-content-center flex-column">
                                <div className="events-countdown">
                                    {zeroPad(minutes)}
                                </div>
                                <div>
                                    Min
                                </div>
                            </div>
                        </div>
                        {/* <div className="events-countdown">{zeroPad(days)} : {zeroPad(hours)} : {zeroPad(minutes)}</div> */}
                    </>
                );
            }
        };
        return (
            <>
                <MDBCarousel
                    activeItem={1}
                    length="3"
                    showControls={false}
                    showIndicators={true}
                    className="z-depth-1"
                    slide
                >
                    <MDBCarouselInner>
                        <MDBCarouselItem itemId="1">
                            <MDBView>
                                <div className="header-image">
                                    <header>
                                        <div className="overlay"></div>
                                        <img src="/assets/img/main_gallery.jpg" className="custom-img align-self-center d-none d-lg-block" alt="" />
                                        <img src="/assets/img/phone_res.jpg" className="custom-img align-self-center d-block d-lg-none" alt="" />
                                        <div className="contents">
                                            <div className="row align-items-end" style={{ height: '50vh' }}>
                                                <div className="col-12 d-flex header-title">
                                                    Our Events
                                                </div>
                                            </div>
                                        </div>
                                    </header>
                                </div>
                            </MDBView>
                        </MDBCarouselItem>
                        <MDBCarouselItem itemId="2">
                            <MDBView>
                                <div className="header-image">
                                    <header>
                                        <div className="overlay"></div>
                                        <img src="/assets/img/main_gallery.jpg" className="custom-img align-self-center d-none d-lg-block" alt="" />
                                        <img src="/assets/img/phone_res.jpg" className="custom-img align-self-center d-block d-lg-none" alt="" />
                                        <div className="contents">
                                            <div className="row align-items-end" style={{ height: '50vh' }}>
                                                <div className="col-12 d-flex header-title">
                                                    Our Events
                                                </div>
                                            </div>
                                        </div>
                                    </header>
                                </div>
                            </MDBView>
                        </MDBCarouselItem>
                        <MDBCarouselItem itemId="3">
                            <MDBView>
                                <div className="header-image">
                                    <header>
                                        <div className="overlay"></div>
                                        <img src="/assets/img/main_gallery.jpg" className="custom-img align-self-center d-none d-lg-block" alt="" />
                                        <img src="/assets/img/phone_res.jpg" className="custom-img align-self-center d-block d-lg-none" alt="" />
                                        <div className="contents">
                                            <div className="row align-items-end" style={{ height: '50vh' }}>
                                                <div className="col-12 d-flex header-title">
                                                    Our Events
                                                </div>
                                            </div>
                                        </div>
                                    </header>
                                </div>
                            </MDBView>
                        </MDBCarouselItem>
                    </MDBCarouselInner>
                </MDBCarousel>

                <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                    <div className="col-12 col-lg-3 seperator-background d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                        <div className="seperator-overlay" style={{ backgroundColor: '#144995' }} />
                        <h2 className="seperator-subtitle align-self-center"><b>Webinar </b>Series</h2>
                    </div>
                    <div className="col-12 col-lg-3 seperator-background d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                        <div className="seperator-overlay" style={{ backgroundColor: '#fec71c' }} />
                        <h2 className="seperator-subtitle align-self-center" style={{ color: '#144995' }}><b>Gelatik</b></h2>
                    </div>
                    <div className="col-12 col-lg-3 seperator-background d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                        <div className="seperator-overlay" style={{ backgroundColor: '#009a77' }} />
                        <h2 className="seperator-subtitle align-self-center"><b>Community</b></h2>
                    </div>
                    <div className="col-12 col-lg-3 seperator-background d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                        <div className="seperator-overlay" style={{ backgroundColor: '#144995' }} />
                        <h2 className="seperator-subtitle align-self-center"><b>School </b>Programs</h2>
                    </div>
                </div>

                <MDBContainer>
                    <div className="row" style={{ margin: '0.01%' }}>
                        <div className="col-lg-12 d-flex justify-content-center flex-column">
                            {/* <div className="events-upcoming align-self-center">Upcoming Events</div> */}
                            <div className="events-title align-self-center">Don't Miss Your Chance to Get Our Events</div>
                            <MDBInputGroup
                                material
                                containerClassName="mb-3 mt-0"
                                hint="Pencarian Events"
                                append={
                                    <MDBBtn
                                        color="dark"
                                        className="m-0 px-3 py-2 z-depth-0"
                                    >
                                        <FontAwesomeIcon icon={faSearch} />
                                    </MDBBtn>
                                }
                            />
                        </div>

                        <div className="col-lg-12 d-flex justify-content-center flex-column">
                            <div className="upcoming-events-title">
                                <div className="row">
                                    <div className="col-lg-2"><FontAwesomeIcon icon={faFire} color="#e25822" /> &nbsp; Upcoming Events</div>
                                    <div className="col-lg-10 paddingNull justify-content-end align-items-end"><hr className="upcoming-events-hr" /></div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-12 d-flex justify-content-center flex-column">
                            <div className="upcoming-events-list">
                                <div className="row">
                                    <div className="col-lg-4 col-md-12">
                                        <MDBCard className="card-events-countdown">
                                            <div className="padding-null">
                                                <div className="events-separator d-flex justify-content-end flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                                                    <div className="events-separator-overlay" style={{ backgroundColor: '#009a77' }} />
                                                    <h6 className="events-separator-title p-4 pl-4">Kadisdik Jabar: Bataru Mudahkan Guru dan Tenaga Kependidikan Miliki Rumah</h6>
                                                </div>
                                            </div>
                                            <MDBCardBody>
                                                <div className="d-flex justify-content-between">
                                                    <p className="events-date dark-grey-text">
                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                    </p>
                                                    <p className="events-view dark-grey-text">
                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views
                                                    </p>
                                                </div>
                                                {
                                                    this.state.isOnline == null ?
                                                        <><h6 style={{ fontWeight: '700', color: '#0057e7' }}>Online</h6></>
                                                        : <><h6 style={{ fontWeight: '700', color: '#d62d20' }}>Closed</h6></>
                                                }
                                                <div className="d-flex justify-content-center">
                                                    <MDBBtn className="register-button" color="warning">
                                                        Daftar
                                                    </MDBBtn>
                                                </div>
                                                <hr />
                                                <div className="d-flex justify-content-center">
                                                    <p className="events-date dark-grey-text">
                                                        <FontAwesomeIcon icon={faClock} /> &nbsp; Akan dimulai dalam :<br />
                                                    </p>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <Countdown
                                                        zeroPadDays={2}
                                                        zeroPadTime={2}
                                                        date={Date.now()}
                                                        renderer={renderer}
                                                        onComplete={() => this.setState({isOnline: false})}
                                                    />
                                                </div>
                                            </MDBCardBody>
                                        </MDBCard>
                                    </div>

                                    <div className="col-lg-4 col-md-12">
                                        <MDBCard className="card-events-countdown">
                                            <div className="padding-null">
                                                <div className="events-separator d-flex justify-content-end flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                                                    <div className="events-separator-overlay" style={{ backgroundColor: '#009a77' }} />
                                                    <h6 className="events-separator-title p-4 pl-4">Kadisdik Jabar: Bataru Mudahkan Guru dan Tenaga Kependidikan Miliki Rumah</h6>
                                                </div>
                                            </div>
                                            <MDBCardBody>
                                                <div className="d-flex justify-content-between">
                                                    <p className="events-date dark-grey-text">
                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                    </p>
                                                    <p className="events-view dark-grey-text">
                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views
                                                    </p>
                                                </div>
                                                {
                                                    this.state.isOnline2 == null ?
                                                        <><h6 style={{ fontWeight: '700', color: '#0057e7' }}>Online</h6></>
                                                        : <><h6 style={{ fontWeight: '700', color: '#d62d20' }}>Closed</h6></>
                                                }
                                                <div className="d-flex justify-content-center">
                                                    <MDBBtn className="register-button" color="warning">
                                                        Daftar
                                                    </MDBBtn>
                                                </div>
                                                <hr />
                                                <div className="d-flex justify-content-center">
                                                    <p className="events-date dark-grey-text">
                                                        <FontAwesomeIcon icon={faClock} /> &nbsp; Akan dimulai dalam :<br />
                                                    </p>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <Countdown
                                                        zeroPadDays={2}
                                                        zeroPadTime={2}
                                                        date={Date.now() + 12000}
                                                        renderer={renderer} 
                                                        onComplete={() => this.setState({isOnline2: false})}
                                                    />
                                                </div>
                                            </MDBCardBody>
                                        </MDBCard>
                                    </div>

                                    <div className="col-lg-4 col-md-12">
                                        <MDBCard className="card-events-countdown">
                                            <div className="padding-null">
                                                <div className="events-separator d-flex justify-content-end flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                                                    <div className="events-separator-overlay" style={{ backgroundColor: '#009a77' }} />
                                                    <h6 className="events-separator-title p-4 pl-4">Kadisdik Jabar: Bataru Mudahkan Guru dan Tenaga Kependidikan Miliki Rumah</h6>
                                                </div>
                                            </div>
                                            <MDBCardBody>
                                                <div className="d-flex justify-content-between">
                                                    <p className="events-date">
                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                    </p>
                                                    <p className="events-view">
                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views
                                                    </p>
                                                </div>
                                                {
                                                    this.state.isOnline3 == null ?
                                                        <><h6 style={{ fontWeight: '700', color: '#0057e7' }}>Online</h6></>
                                                        : <><h6 style={{ fontWeight: '700', color: '#d62d20' }}>Closed</h6></>
                                                }
                                                <div className="d-flex justify-content-center">
                                                    <MDBBtn className="register-button" color="warning">
                                                        Daftar
                                                    </MDBBtn>
                                                </div>
                                                <hr />
                                                <div className="d-flex justify-content-center">
                                                    <p className="events-date">
                                                        <FontAwesomeIcon icon={faClock} /> &nbsp; Akan dimulai dalam :<br />
                                                    </p>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <Countdown
                                                        zeroPadDays={2}
                                                        zeroPadTime={2}
                                                        date={Date.now() + 13000} renderer={renderer} 
                                                        onComplete={() => this.setState({isOnline3: false})}
                                                    />
                                                </div>
                                            </MDBCardBody>
                                        </MDBCard>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-12 d-flex justify-content-center flex-column">
                            <div className="events-header">
                                <div className="row">
                                    <div className="col-lg-8">
                                        <div className="row">
                                            <div className="col-lg-3 d-flex justify-content-start align-items-center">
                                                <b>Webinar</b> &nbsp; Series
                                            </div>
                                            <div className="col-lg-9 d-flex justify-content-start align-items-center">
                                                <MDBInputGroup
                                                    material
                                                    prepend={
                                                        <MDBBtn
                                                            color="warning"
                                                            className="m-0 px-3 py-2 z-depth-0"
                                                        >
                                                            <FontAwesomeIcon icon={faSearch} />
                                                        </MDBBtn>
                                                    }
                                                />
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center mb-4">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-end align-items-center">
                                                <MDBPagination color="dark">
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link">
                                                            <span>First</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                            <span className="sr-only">Previous</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem active>
                                                        <MDBPageNav className="page-link">
                                                            1 <span className="sr-only">(current)</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            2
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            3
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            4
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            5
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            &raquo;
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            Last
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                </MDBPagination>
                                            </div>

                                            <div className="col-lg-3 d-flex justify-content-start align-items-center">
                                                Gelatik
                                            </div>
                                            <div className="col-lg-9 d-flex justify-content-start align-items-center">
                                                <MDBInputGroup
                                                    material
                                                    prepend={
                                                        <MDBBtn
                                                            color="warning"
                                                            className="m-0 px-3 py-2 z-depth-0"
                                                        >
                                                            <FontAwesomeIcon icon={faSearch} />
                                                        </MDBBtn>
                                                    }
                                                />
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center mb-4">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-end align-items-center">
                                                <MDBPagination color="dark">
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link">
                                                            <span>First</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                            <span className="sr-only">Previous</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem active>
                                                        <MDBPageNav className="page-link">
                                                            1 <span className="sr-only">(current)</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            2
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            3
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            4
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            5
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            &raquo;
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            Last
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                </MDBPagination>
                                            </div>

                                            <div className="col-lg-3 d-flex justify-content-start align-items-center">
                                                Community
                                            </div>
                                            <div className="col-lg-9 d-flex justify-content-start align-items-center">
                                                <MDBInputGroup
                                                    material
                                                    prepend={
                                                        <MDBBtn
                                                            color="warning"
                                                            className="m-0 px-3 py-2 z-depth-0"
                                                        >
                                                            <FontAwesomeIcon icon={faSearch} />
                                                        </MDBBtn>
                                                    }
                                                />
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center mb-4">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-end align-items-center">
                                                <MDBPagination color="dark">
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link">
                                                            <span>First</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                            <span className="sr-only">Previous</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem active>
                                                        <MDBPageNav className="page-link">
                                                            1 <span className="sr-only">(current)</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            2
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            3
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            4
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            5
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            &raquo;
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            Last
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                </MDBPagination>
                                            </div>

                                            <div className="col-lg-3 d-flex justify-content-start align-items-center">
                                                School Programs
                                            </div>
                                            <div className="col-lg-9 d-flex justify-content-start align-items-center">
                                                <MDBInputGroup
                                                    material
                                                    prepend={
                                                        <MDBBtn
                                                            color="warning"
                                                            className="m-0 px-3 py-2 z-depth-0"
                                                        >
                                                            <FontAwesomeIcon icon={faSearch} />
                                                        </MDBBtn>
                                                    }
                                                />
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center mb-4">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-start align-items-center">
                                                <Card style={{ display: 'flex', width: '100%', minHeight: '15vh' }}>
                                                    <CardMedia
                                                        style={{ minWidth: '15vw' }}
                                                        image="https://mdbootstrap.com/img/Photos/Others/images/49.jpg"
                                                        title="Live from space album cover"
                                                    />
                                                    <div className="events">
                                                        <CardContent className="events-content">
                                                            {string.substring(0, 150) + '. . . '}
                                                        </CardContent>
                                                        <div className="events-footer">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} /> &nbsp; 30 Maret 2021, 08:00 WIB
                                                                    </p>
                                                                    <p className="events-date">
                                                                        <FontAwesomeIcon icon={faEye} /> &nbsp; 93 views &nbsp; &nbsp;
                                                                        <FontAwesomeIcon icon={faTags} /> &nbsp; Webinar Series
                                                                    </p>
                                                                </div>
                                                                <div className="col-6 d-flex justify-content-end align-items-center">
                                                                    <MDBBtn className="register-button" color="warning">
                                                                        Daftar
                                                                    </MDBBtn>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>

                                            <div className="col-lg-12 d-flex justify-content-end align-items-center">
                                                <MDBPagination color="dark">
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link">
                                                            <span>First</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem disabled>
                                                        <MDBPageNav className="page-link" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                            <span className="sr-only">Previous</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem active>
                                                        <MDBPageNav className="page-link">
                                                            1 <span className="sr-only">(current)</span>
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            2
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            3
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            4
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            5
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            &raquo;
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                    <MDBPageItem>
                                                        <MDBPageNav className="page-link">
                                                            Last
                                                        </MDBPageNav>
                                                    </MDBPageItem>
                                                </MDBPagination>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-4">
                                        <div className="row">
                                            <div className="col-12 d-flex justify-content-start align-items-center">
                                                <img src="https://i.pinimg.com/564x/9e/23/0f/9e230f0ed3b7b18489d4a395ac78af94.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                                            </div>
                                            <div className="col-12 d-flex justify-content-start align-items-center mt-2">
                                                <MDBCard>
                                                    <MDBCardBody>
                                                        <div className="input-group mb-3">
                                                            <input type="text" className="form-control" placeholder="Pencarian Berita" aria-label="Username" aria-describedby="basic-addon"  />
                                                            <div className="input-group-prepend">
                                                                <span className="input-group-text" id="basic-addon">
                                                                    <FontAwesomeIcon icon={faSearch}/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <MDBCardTitle>Berita Terbaru</MDBCardTitle>
                                                        <hr/>
                                                        <MDBCardText>
                                                            <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                                                            <div className="news-title">
                                                                Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                                                            <div className="news-content">
                                                                {string.substring(0, 150) + '. . . '} <LinkTo style={{color: '#d62d20'}}>Keep Reading...</LinkTo>
                                                            </div>
                                                        </MDBCardText>
                                                        <hr/>
                                                        <MDBCardText>
                                                            <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                                                            <div className="news-title">
                                                                Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                                                            <div className="news-content">
                                                                {string.substring(0, 150) + '. . . '} <LinkTo style={{color: '#d62d20'}}>Keep Reading...</LinkTo>
                                                            </div>
                                                        </MDBCardText>
                                                        <hr/>
                                                        <MDBCardText>
                                                            <img className="mb-3" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '15vh' }} />
                                                            <div className="news-title">
                                                                Renja Pendidikan Jabar Harus Kembangkan Inovasi
                                                            </div>
                                                            <div className="news-content">
                                                                {string.substring(0, 150) + '. . . '} <LinkTo style={{color: '#d62d20'}}>Keep Reading...</LinkTo>
                                                            </div>
                                                            
                                                        </MDBCardText>
                                                        <MDBCardText>
                                                            <div className="news-all">
                                                                <LinkTo style={{color: '#0057e7'}}>Lihat Semua Berita <FontAwesomeIcon icon={faChevronCircleRight}/></LinkTo>
                                                            </div>
                                                        </MDBCardText>
                                                    </MDBCardBody>
                                                </MDBCard>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </MDBContainer>
            </>
        );
    }
};
export default MainEvents;
