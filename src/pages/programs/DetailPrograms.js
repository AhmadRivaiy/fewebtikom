import React from 'react';
import {
    MDBContainer,
    MDBCard, MDBCardBody, MDBCardTitle, MDBTabPane, MDBTabContent,
    MDBRow, MDBCol,
    MDBAnimation, MDBCarousel, MDBCarouselInner, MDBCarouselItem, MDBView
} from 'mdbreact';
import { Link as LinkTo } from 'react-router-dom';
import '../../assets/css/Programs.css';
import { Events } from 'react-scroll'
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt,  faDiceOne,  faDiceThree,  faDiceTwo,  faEye, faTags, faUser } from '@fortawesome/free-solid-svg-icons'

class DetailPrograms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data_about: [],
            is_lang: [!localStorage.getItem('is_lang_now') ? 0 : JSON.parse(localStorage.getItem('is_lang_now')).param],
            options: [
                {
                    text: "Option 1",
                    value: "1"
                },
                {
                    text: "Option 2",
                    value: "2"
                },
                {
                    text: "Option 3",
                    value: "3"
                }
            ],
            modalDetailPhotos: false,
            pictureContent: true,
            videoContent: false,
            stateContent: 'photo',
            title: '',
            pageCuk: 0,
            activeItemCaution: "1"
        };
    }

    toggle(param) {
        this.setState({
            modalDetailPhotos: !this.state.modalDetailPhotos,
            title: param
        });
    }

    contentSwitch(param) {
        const { stateContent, pictureContent, videoContent } = this.state;
        if (param != stateContent) {
            this.setState({
                stateContent: param,
                pictureContent: !pictureContent,
                videoContent: !videoContent
            });
        }

    }

    //scrollToTop = () => window.scrollTo(0, 0);

    async componentDidMount() {
        window.scrollTo(0, 0);
        console.log(this.props.match.params.tag)
        Events.scrollEvent.register('begin');
        Events.scrollEvent.register('end');
    }

    componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
    }

    toggleBottom(tab) {
        if (this.state.activeItemCaution !== tab) {
          var num = tab + 1;
          var n = num.toString();
          this.setState({
            activeItemCaution: n,
            pageCuk: tab
          });
        }
      };

    render() {
        const { t } = this.props;
        const { activeItemCaution, pageCuk } = this.state;
        const someString = '1 Lorem ipssdum dolor sdsit ametsd, conasdsadsectetur adipisicing elit. Nihsadilsd odit magnam msama, soluta doloribus reiciendis minima.';
        return (
            <>
                <MDBCarousel
                            activeItem={1}
                            length={3}
                            showControls={true}
                            showIndicators={true}
                            className="z-depth-1"
                        >
                            <MDBCarouselInner>
                                <MDBCarouselItem itemId="1">
                                    <MDBView>
                                    <img
                                        className="d-block w-100"
                                        src="https://mdbootstrap.com/img/Photos/Slides/img%20(130).jpg"
                                        alt="First slide"
                                    />
                                    </MDBView>
                                </MDBCarouselItem>
                                <MDBCarouselItem itemId="2">
                                    <MDBView>
                                    <img
                                        className="d-block w-100"
                                        src="https://mdbootstrap.com/img/Photos/Slides/img%20(129).jpg"
                                        alt="Second slide"
                                    />
                                    </MDBView>
                                </MDBCarouselItem>
                                <MDBCarouselItem itemId="3">
                                    <MDBView>
                                    <img
                                        className="d-block w-100"
                                        src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg"
                                        alt="Third slide"
                                    />
                                    </MDBView>
                                </MDBCarouselItem>
                            </MDBCarouselInner>
                        </MDBCarousel>
                <MDBContainer size="lg">
                    <MDBView className="title-detail-content mt-5">
                        <h1>e-Learning</h1>
                    </MDBView>
                    <MDBView>
                        <div className="row mt-2 subtitle-detail-content">
                            <div className="col-lg-auto col-md-auto col-sm-6">
                                <span><FontAwesomeIcon icon={faUser}/></span> &nbsp;Oleh Tikompedia
                            </div>
                            <div className="col-lg-auto col-md-auto col-sm-6">
                                <span><FontAwesomeIcon icon={faCalendarAlt}/></span> &nbsp;24 Maret, 14:30 WIB
                            </div>
                            <div className="col-lg-auto col-md-auto col-sm-6">
                                <span><FontAwesomeIcon icon={faEye}/></span> &nbsp;90 Views
                            </div>
                            <div className="col-lg-auto col-md-auto col-sm-6">
                                <span><FontAwesomeIcon icon={faTags} flip="horizontal"/></span> &nbsp;Education, Technology
                            </div>
                        </div>
                    </MDBView>
                    <div style={{ marginTop: '5vh', minHeight: '60vh' }}>
                        <div className="row">
                            <div className="col-lg-7 col-sm-12 content-detail-content">
                                Quisquam aperiam, pariatur. Tempora, placeat ratione porro
                                voluptate odit minima. Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Nihil odit magnam minima,
                                soluta doloribus reiciendis molestiae placeat unde eos
                                molestias.
                            </div>
                            <div className="col-lg-5 col-sm-12" style={{ display: 'grid', minWidth: 400, maxWidth: 200 }}>
                                <MDBCard>
                                    <MDBCardBody>
                                    <div>
                                        <MDBCardTitle className="blue-text">Other Programs</MDBCardTitle><hr></hr>
                                        <MDBTabContent activeItem={activeItemCaution}>
                                            <MDBTabPane tabId="1" role="tabpanel">
                                                <MDBAnimation type="fadeIn">
                                                    <MDBRow className="side-menu-card">
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                    </MDBRow>
                                                </MDBAnimation>
                                            </MDBTabPane>
                                            <MDBTabPane tabId="2" role="tabpanel">
                                                <MDBAnimation type="fadeIn">
                                                    <MDBRow className="side-menu-card">
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                    </MDBRow>
                                                </MDBAnimation>
                                            </MDBTabPane>
                                            <MDBTabPane tabId="3" role="tabpanel">
                                                <MDBAnimation type="fadeIn">
                                                    <MDBRow className="side-menu-card">
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                        <MDBCol size="12">
                                                            <div className="title">What This Is Title</div>
                                                            <div className="content">
                                                                <p>
                                                                    { someString.substring(0, 100) + '. . . ' }
                                                                    <LinkTo to="/side-detail" className="link-more">Keep Reading &gt;&gt;&gt;</LinkTo>
                                                                </p>
                                                            </div>
                                                            <hr></hr>
                                                        </MDBCol>
                                                    </MDBRow>
                                                </MDBAnimation>
                                            </MDBTabPane>
                                        </MDBTabContent>
                                    </div>
                                    <div>
                                        <BottomNavigation
                                            value={pageCuk}
                                            showLabels
                                            onChange={(event, newValue) => {
                                                this.toggleBottom(newValue)
                                            }}
                                            >
                                            <BottomNavigationAction label="Recents" icon={<FontAwesomeIcon icon={faDiceOne} />} />
                                            <BottomNavigationAction label="Favorites" icon={<FontAwesomeIcon icon={faDiceTwo} />} />
                                            <BottomNavigationAction label="Nearby" icon={<FontAwesomeIcon icon={faDiceThree} />} />
                                        </BottomNavigation>
                                    </div>
                                    </MDBCardBody>
                                </MDBCard>
                            </div>
                        </div>
                    </div>
                </MDBContainer>
                <MDBView>
                    <div className="row title-upcoming" style={{ marginTop: '15vh' }}>
                        <div className="col-lg-1 col-sm-12" style={{ backgroundColor: '#fec71c' }} />
                        <div className="col-lg-2 col-sm-12 sub-title">
                            Lates News
                        </div>
                        <div className="col-lg-8 col-sm-12 d-flex align-items-center">
                            <button className="btn btn-outline-warning" style={{ fontSize: '1rem' }}>Selengkapnya</button>
                        </div>
                    </div>
                    <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-yellow" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-blue" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-green" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                    </div>
                </MDBView>
            </>
        );
    }
};
export default DetailPrograms;
