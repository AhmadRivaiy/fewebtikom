import React from 'react';
import {
    MDBContainer,
    MDBBtn,
    MDBCol,
    MDBAnimation, MDBView, MDBInputGroup, MDBPagination, MDBPageItem, MDBPageNav
} from 'mdbreact';
import { Link as LinkTo } from 'react-router-dom';
import '../../assets/css/Programs.css';
import { Events } from 'react-scroll'
import { Slide } from 'react-slideshow-image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClone, faSearch } from '@fortawesome/free-solid-svg-icons'

class MainPrograms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data_about: [],
            is_lang: [!localStorage.getItem('is_lang_now') ? 0 : JSON.parse(localStorage.getItem('is_lang_now')).param],
            options: [
                {
                    text: "Option 1",
                    value: "1"
                },
                {
                    text: "Option 2",
                    value: "2"
                },
                {
                    text: "Option 3",
                    value: "3"
                }
            ],
            modalDetailPhotos: false,
            pictureContent: true,
            videoContent: false,
            stateContent: 'photo',
            title: ''
        };
    }

    toggle(route) {
        this.props.history.push(route);
    }

    contentSwitch(param) {
        const { stateContent, pictureContent, videoContent } = this.state;
        if (param != stateContent) {
            this.setState({
                stateContent: param,
                pictureContent: !pictureContent,
                videoContent: !videoContent
            });
        }

    }

    scrollToTop = () => window.scrollTo(0, 0);

    async componentDidMount() {
        Events.scrollEvent.register('begin');
        Events.scrollEvent.register('end');
    }

    componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
    }

    render() {
        const { t } = this.props;
        const { title, modalDetailPhotos, pictureContent, videoContent } = this.state;
        const pictureCount = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        return (
            <>
                <div className="header-image">
                    <header>
                        <div className="overlay" style={{ backgroundColor: '#164d9f' }}></div>
                        <img src="/assets/img/main_gallery.jpg" className="custom-img align-self-center d-none d-lg-block" alt="" />
                        <img src="/assets/img/phone_res.jpg" className="custom-img align-self-center d-block d-lg-none" alt="" />
                        <div className="contents">
                            <div className="row align-items-end" style={{ height: '50vh' }}>
                                <div className="col-12 d-flex title-header">
                                    Our Programs
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
                <MDBView>
                    <MDBContainer>
                        <div className="col-lg-12 d-flex justify-content-center flex-column">
                            <div className="impression-title align-self-center">What Can We Do For You?</div>
                            <MDBInputGroup
                                material
                                containerClassName="mb-3 mt-3"
                                hint="Pencarian Programs"
                                append={
                                    <MDBBtn
                                        color="grey"
                                        className="m-0 px-3 py-2 z-depth-0"
                                    >
                                        <FontAwesomeIcon icon={faSearch} />
                                    </MDBBtn>
                                }
                            />
                        </div>
                        <div className="row" style={{ marginTop: '9vh', position: 'relative', marginRight: '0.01%' }}>
                            <div className="col-12 d-flex justify-content-end align-items-center">
                                Urutkan Berdasarkan
                                        <div className="col-lg-3">
                                    <div className="select">
                                        <select>
                                            <option>Terbaru</option>
                                            <option>Terlama</option>
                                        </select>
                                        <div className="select_arrow">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </MDBContainer>
                </MDBView>
                <MDBView className="ml-4 mr-4">
                    <MDBContainer>
                        <div className="row">
                            {
                                pictureCount.map((x, key) => {
                                    return (
                                        <div className="col-sm-12 col-lg-4 padding-null" key={key} onClick={() => this.toggle('/main-programs/' + 'new-building-at-this-year-2021')}>
                                            <MDBAnimation type="fadeIn">
                                                <div className="col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                                                    {key % 2 == 0 ?
                                                        <div className="overlay-col-separator-yellow" />
                                                        :
                                                        key % 3 == 1 ? <div className="overlay-col-separator-green" />
                                                            : <div className="overlay-col-separator-blue" />}
                                                    <h4 className="col-separator-subtitle align-self-center">Everybody Get Up and Sleep Again</h4>
                                                </div>
                                            </MDBAnimation>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <div className="d-flex justify-content-center align-items-center pt-5">
                            <MDBPagination circle color="amber">
                                <MDBPageItem disabled>
                                    <MDBPageNav className="page-link">
                                    <span>First</span>
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem disabled>
                                    <MDBPageNav className="page-link" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span className="sr-only">Previous</span>
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem active>
                                    <MDBPageNav className="page-link">
                                    1 <span className="sr-only">(current)</span>
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem>
                                    <MDBPageNav className="page-link">
                                    2
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem>
                                    <MDBPageNav className="page-link">
                                    3
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem>
                                    <MDBPageNav className="page-link">
                                    4
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem>
                                    <MDBPageNav className="page-link">
                                    5
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem>
                                    <MDBPageNav className="page-link">
                                    &raquo;
                                    </MDBPageNav>
                                </MDBPageItem>
                                <MDBPageItem>
                                    <MDBPageNav className="page-link">
                                    Last
                                    </MDBPageNav>
                                </MDBPageItem>
                            </MDBPagination>
                        </div>
                    </MDBContainer>
                </MDBView>
                <MDBView style={{  marginRight: '0.01%' }}>
                    <div className="row title-upcoming" style={{ marginTop: '15vh' }}>
                        <div className="col-lg-1 col-sm-12" style={{ backgroundColor: '#fec71c' }} />
                        <div className="col-lg-2 col-sm-12 sub-title">
                            Lates News
                        </div>
                        <div className="col-lg-8 col-sm-12 d-flex align-items-center">
                            <button className="btn btn-outline-warning" style={{ fontSize: '1rem' }}>Selengkapnya</button>
                        </div>
                    </div>
                    <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-yellow" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-blue" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-green" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                    </div>
                </MDBView>
            </>
        );
    }
};
export default MainPrograms;
