import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBCarouselInner, MDBCarouselItem, MDBView, MDBCarousel } from "mdbreact";

class Partners extends React.Component {
    render() {
        return (
            <>
                <div className="header-image">
                    <header>
                        <div className="overlay"></div>
                        <img src="/assets/img/main_gallery.jpg" className="custom-img align-self-center d-none d-lg-block" alt="" />
                        <img src="/assets/img/phone_res.jpg" className="custom-img align-self-center d-block d-lg-none" alt="" />
                        <div className="contents">
                            <div className="row align-items-end" style={{ height: '50vh' }}>
                                <div className="col-12 d-flex title-header">
                                    Our Partners
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
                <MDBContainer className="mt-5">
                    <MDBRow>
                        <MDBCol lg="4" md="6" className="mb-4">
                            <img src="https://yt3.ggpht.com/ytc/AAUvwng0qMfptl-VwHH-5pW1qWqxWaH88_NSNtyslCrD=s900-c-k-c0x00ffffff-no-rj" className="img-fluid" alt="SEAMOLEC" width="250" height="200" />
                        </MDBCol>
                        <MDBCol lg="4" md="6" className="mb-4">
                            <img src="https://pijarsekolah.id/wp-content/uploads/2020/03/pijarlogo.png" className="img-fluid" altalt="PIJAR" width="250" height="200" />
                        </MDBCol>
                        <MDBCol lg="4" md="6" className="mb-4">
                            <img src="https://logoeps.com/wp-content/uploads/2012/07/university-of-cambridge-logo-vector-01.png" className="img-fluid" altalt="Cambridge University" width="250" height="200" />
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <div className="mt-5">
                <MDBView>
                    <div className="row title-upcoming" style={{ marginTop: '15vh' }}>
                        <div className="col-1" style={{ backgroundColor: '#fec71c' }} />
                        <div className="col-2 sub-title">
                            Lates News
                        </div>
                        <div className="col-8 d-flex align-items-center">
                            <button className="btn btn-outline-warning w-25" style={{ fontSize: '1rem' }}>Selengkapnya</button>
                        </div>
                    </div>
                    <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-yellow" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-blue" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-green" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                    </div>
                </MDBView>
                </div>
            </>

        );
    }
}

export default Partners;
