import React from 'react';
import {
  MDBContainer,
  MDBBtn,
  MDBCol,
  MDBRow,
  MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink,
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBView,
  MDBTypography,
  MDBAnimation,
  MDBPagination,
  MDBPageItem,
  MDBPageNav
} from 'mdbreact';
import '../../assets/css/HomePage.css';
import imgHendra from '../../assets/unnamed.jpg';
import Typed from 'react-typed';
import { Link, Element, Events } from 'react-scroll';
import { Slide } from 'react-slideshow-image';
import Card from 'react-bootstrap/Card'
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import { Link as LinkTo } from 'react-router-dom';

import videoExample from '../../assets/sample_video2.mp4'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class HomePage extends React.Component {
  state = {
    activeItem: "1",
    pageCuk: 0,
    activeItemCaution: "1"
  };

  toggle(tab) {
    if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
    }
  };

  linkTo(route) {
    this.props.history.push(route);
  }

  toggleBottom(tab) {
    if (this.state.activeItemCaution !== tab) {
      var num = tab + 1;
      var n = num.toString();
      this.setState({
        activeItemCaution: n,
        pageCuk: tab
      });
    }
  };

  scrollToTop = () => window.scrollTo(0, 0);

  componentDidMount() {
    Events.scrollEvent.register('begin');
    Events.scrollEvent.register('end');

  }

  componentWillUnmount() {
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  }

  render() {
    const { pageCuk } = this.state;
    const properties = {
      duration: 5000,
      transitionDuration: 500,
      infinite: true,
      indicator: true,
      prevArrow: <div style={{ width: "30px", marginRight: "-30px", color: 'snow', fontSize: '2rem', cursor: 'pointer' }}><i className="fas fa-chevron-left ml-3"></i></div>,
      nextArrow: <div style={{ width: "30px", marginLeft: "-30px", color: 'snow', fontSize: '2rem', cursor: 'pointer' }}><i className="fas fa-chevron-right"></i></div>,
      indicators: i => (<div className="indicator"></div>)
    };
    const slideEvents = [
      {
        image: 'https://www.kemenkumham.go.id/images/jux_portfolio_pro/pusdatin_new.png',
        deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
        link: 'https://www.kemenkumham.go.id/publikasi/logo'
      },
      {
        image: 'https://assets.pikiran-rakyat.com/crop/0x393:3507x2707/x/photo/2020/11/24/4081988142.png',
        deskripsi: 'Pelaksanaan Upacara Bendera Peringatan Hari Guru Nasional 2020 tersebut dilaksanakan pada Rabu, 25 November 2020 mulai pukul 08:00 WIB, tidak hanya itu, dalam surat yang dikeluarkan Mendikbud tersebut juga menghimbau pelaksanaan upacara bendera harus menerapkan Protokol kesehatan Covid-19.',
        link: 'https://dialektikakuningan.pikiran-rakyat.com/pendidikan/pr-651009523/kemdikbud-imbau-masyarakat-meriahkan-hari-guru-nasional-2020-download-logo-resminya-di-sini'
      },
      {
        image: 'https://static.vecteezy.com/system/resources/previews/001/190/646/original/flower-watercolor-png.png',
        deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
        link: '#'
      },
      {
        image: 'https://www.phiradio.net/wp-content/uploads/2020/02/logo_tikomdik.png',
        deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
        link: '#'
      },
      {
        image: 'https://gerecep.cadisdikwil7.id/assets/media/logos/logo-gerecep.png',
        deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
        link: '#'
      }
    ];
    const someString = '1 Lorem ipssdum dolor sdsit ametsd, conasdsadsectetur adipisicing elit. Nihsadilsd odit magnam msama, soluta doloribus reiciendis minima.';

    return (
      <>
        <MDBView
          className="moving-text d-flex justify-content-center d-block d-lg-none">
          <h1 className="align-self-center" style={{ fontWeight: 'bold', color: 'white', fontSize: 52 }}>
            <Typed
              strings={[
                'Creative',
                'Innovative',
                'Collaborative']}
              typeSpeed={40}
              backSpeed={70}
              showCursor={false}
              loop>
            </Typed>
          </h1>
        </MDBView>
        <MDBCarousel
          activeItem={1}
          length={3}
          showControls={false}
          showIndicators={false}
          mobileGesture
          className='corrousel-render-phone z-depth-1 d-block d-lg-none'
        >
          <MDBCarouselInner>
            <MDBCarouselItem itemId='1'>
              <MDBView>
                <img
                  className='d-block w-100'
                  src='https://images.pexels.com/photos/719396/pexels-photo-719396.jpeg'
                  alt='First slide'
                />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId='2'>
              <MDBView>
                <img
                  className='d-block w-100'
                  src='https://images.unsplash.com/photo-1497250681960-ef046c08a56e?ixlib=rb-1.2.1'
                  alt='Second slide'
                />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId='3'>
              <MDBView>
                <img
                  className='d-block w-100'
                  src='https://images.pexels.com/photos/1236701/pexels-photo-1236701.jpeg'
                  alt='Third slide'
                />
              </MDBView>
            </MDBCarouselItem>
          </MDBCarouselInner>
        </MDBCarousel>
        <header className="d-none d-lg-block">
          <div className="overlay"></div>
          <div className="d-flex contents" style={{ color: '#0c3974' }}>
            <div className="d-flex flex-column">
              <div className="font-banner">
                Creative, Innovative<br /> Collaborative<br />
                <b className="font-banner-tikomdik">Tikomdik</b> <Typed
                  strings={[
                    'Empowers To Educate']}
                  typeSpeed={40}
                  backSpeed={70}
                  startDelay={80}
                  showCursor={false}
                  loop>
                </Typed>
              </div>
              <div>
                <Link activeClass="active" to="moreWelcome" offset={-25} spy={true} smooth={true} duration={1200} >
                  <button className="rounded-warning">Explore</button>
                </Link>
              </div>
            </div>
          </div>
          <video className='d-block shadow' autoPlay muted loop style={{ opacity: 0.7 }}>
            <source
              src={videoExample}
              type='video/mp4'
            />
          </video>
        </header>
        <Element name="moreWelcome" className="element bannerOne" >
          <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
            <div className="col-sm-12 col-lg-5 col-separator-home d-flex justify-content-center flex-column">
              <h2 className="sub-title-separator align-self-center align-content-center">Lihat apa yang Terjadi <b>Baca Berita Terbaru</b></h2>
            </div>
            <div className="col-lg-2 col-separator-home">
            </div>
            <div className="col-sm-12 col-lg-5 col-separator-home d-flex justify-content-center flex-column">
              <button className="align-self-center rounded-warning long-width">View all Lates News</button>
            </div>
          </div>
        </Element>
        <div className="d-none d-lg-block">
          <div className="container-fluid flex-row tabs-news">
            <div>
              <MDBNav>
                <MDBNavItem>
                  <MDBNavLink link to="#" active={this.state.activeItem === "1"} onClick={() => this.toggle("1")} role="tab" className="navLink1">
                    Education
                  </MDBNavLink>
                </MDBNavItem>
                <MDBNavItem>
                  <MDBNavLink link to="#" active={this.state.activeItem === "2"} onClick={() => this.toggle("2")} role="tab" className="navLink2" >
                    Technology
                  </MDBNavLink>
                </MDBNavItem>
                <MDBNavItem>
                  <MDBNavLink link to="#" active={this.state.activeItem === "3"} onClick={() => this.toggle("3")} role="tab" className="navLink3" >
                    System/Policy
                  </MDBNavLink>
                </MDBNavItem>
              </MDBNav>
              <MDBTabContent activeItem={this.state.activeItem}>
                <MDBTabPane tabId="1" role="tabpanel" style={{ backgroundColor: '#fec71c', borderColor: '#fec71c', padding: 20 }}>
                  <MDBAnimation type="fadeIn">
                    <img src={imgHendra} className="img-fluid" width="100%" height="auto" alt="Pak Hendra" />
                    <p className="textCaption">
                      1 Lorem ipssdum dolor sdsit ametsd, conasdsadsectetur adipisicing elit.
                      Nihsadilsd odit magnam msama, soluta doloribus reiciendis
                      molestiae plasadt unde eos molestias. Quisquam aperiam,
                      pariatur. Tempora, placeat ratione porro voluptate odit
                      minima.
                      </p>
                  </MDBAnimation>
                </MDBTabPane>
                <MDBTabPane tabId="2" role="tabpanel" style={{ backgroundColor: '#164d9f', borderColor: '#164d9f', padding: 20 }}>
                  <MDBAnimation type="fadeIn">
                    <img src="https://static.timesofisrael.com/www/uploads/2019/01/iStock-903073904-e1548155631198.jpg" className="img-fluid" width="100%" height="auto" alt="Pak Hendra" />
                    <p className="textCaption">
                      Quisquam aperiam, pariatur. Tempora, placeat ratione porro
                      voluptate odit minima. Lorem ipsum dolor sit amet,
                      consectetur adipisicing elit. Nihil odit magnam minima,
                      soluta doloribus reiciendis molestiae placeat unde eos
                      molestias.
                  </p>
                  </MDBAnimation>
                </MDBTabPane>
                <MDBTabPane tabId="3" role="tabpanel" style={{ backgroundColor: '#009a77', borderColor: '#009a77', padding: 20 }}>
                  <MDBAnimation type="fadeIn">
                    <img src="https://nomadiclearning.com/img/asset/bWFpbi9pbWcvaVN0b2NrLTYzOTgyMDg4OC5qcGc=?s=dd66727d08b4eed69c85473ad1e8926d" className="img-fluid" width="100%" height="auto" alt="Pak Hendra" />
                    <p className="textCaption">
                      Quisquam aperiam, pariatur. Tempora, placeat ratione porro
                      voluptate odit minima. Lorem ipsum dolor sit amet,
                      consectetur adipisicing elit. Nihil odit magnam minima,
                      soluta doloribus reiciendis molestiae placeat unde eos
                      molestias.
                  </p>
                  </MDBAnimation>
                </MDBTabPane>
              </MDBTabContent>
            </div>
            <div className="mt-5 ml-4" style={{ display: 'grid', minWidth: 450, maxWidth: 200 }}>
            <div className="row card-side-programs">
                <div className="col-12 d-flex justify-content-start align-items-center mt-2">
                  <Card>
                    <Card.Body>
                      <MDBCardTitle>Program</MDBCardTitle>
                      <hr />
                      <div>
                        <img className="mb-3 img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '5vh' }} />
                        <div className="news-title">Renja Pendidikan Jabar Harus Kembangkan Inovasi</div>
                        <div className="news-content">
                          {someString.substring(0, 150) + '. . . '} <LinkTo to="#" style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                        </div>
                      </div>
                      <hr />
                      <div>
                        <img className="mb-3 img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" style={{ width: '100%', minHeight: '5vh' }} />
                        <div className="news-title">
                          Renja Pendidikan Jabar Harus Kembangkan Inovasi
                        </div>
                        <div className="news-content">
                          {someString.substring(0, 150) + '. . . '} <LinkTo to="#" style={{ color: '#d62d20' }}>Keep Reading...</LinkTo>
                        </div>
                      </div>
                    </Card.Body>
                    <Card.Footer>
                      <div className="col-lg-12 d-flex justify-content-end align-items-center">
                        <MDBPagination color="blue">
                          <MDBPageItem disabled>
                            <MDBPageNav className="page-link">
                              <span>First</span>
                            </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem disabled>
                            <MDBPageNav className="page-link" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                              <span className="sr-only">Previous</span>
                            </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem active>
                            <MDBPageNav className="page-link">
                              1 <span className="sr-only">(current)</span>
                            </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem>
                            <MDBPageNav className="page-link">
                              2
                                                        </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem>
                            <MDBPageNav className="page-link">
                              3
                                                        </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem>
                            <MDBPageNav className="page-link">
                              4
                                                        </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem>
                            <MDBPageNav className="page-link">
                              5
                                                        </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem>
                            <MDBPageNav className="page-link">
                              &raquo;
                                                        </MDBPageNav>
                          </MDBPageItem>
                          <MDBPageItem>
                            <MDBPageNav className="page-link">
                              Last
                                                        </MDBPageNav>
                          </MDBPageItem>
                        </MDBPagination>
                      </div>
                    </Card.Footer>
                  </Card>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style={{ height: '100vh', marginTop: '15vh' }} />
        <div className='mb-5'>
          <MDBCarousel
            activeItem={2}
            length={3}
            showControls={true}
            onHoverStop={true}
            interval={3000}
            className="z-depth-1 mt-4 black"
            showIndicators={true}
            slide
          >
            <MDBCarouselInner>
              <MDBCarouselItem itemId="1">
                <MDBView>
                  <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-yellow" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-blue" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-green" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                  </div>
                </MDBView>
              </MDBCarouselItem>
              <MDBCarouselItem itemId="2">
                <MDBView>
                  <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-yellow" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-blue" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-green" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                  </div>
                </MDBView>
              </MDBCarouselItem>
              <MDBCarouselItem itemId="3">
                <MDBView>
                  <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-yellow" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-blue" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                    <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                      <div className="overlay-col-separator-green" />
                      <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                    </div>
                  </div>
                </MDBView>
              </MDBCarouselItem>
            </MDBCarouselInner>
          </MDBCarousel>

          { /* JARGON */}
          <MDBView>
            <div className="row justify-content-end mt-4">
              <div className="col-12 line-paragrap-jargon">
                <MDBAnimation reveal type='fadeInRight' duration="2000ms">
                  <div className="margin-jargon-right font-jargon">
                    Creative
                  </div>
                </MDBAnimation>
                <div className="col-12" style={{ backgroundColor: '#f2f2f2' }}>
                  <MDBAnimation reveal type='fadeInLeft' duration="2000ms">
                    <div className="margin-jargon-right font-jargon">
                      Innovative
                    </div>
                  </MDBAnimation>
                  <MDBAnimation reveal type='fadeInRight' duration="2000ms">
                    <div className="margin-jargon-right font-jargon">
                      Collaborative
                    </div>
                  </MDBAnimation>
                  <MDBAnimation reveal type='fadeInLeft' duration="2000ms">
                    <div className="margin-jargon-right font-jargon-tikom">
                      TIKomDik
                    </div>
                  </MDBAnimation>
                </div>
                <MDBAnimation reveal type='fadeIn' duration="2000ms">
                  <div className="col-12 font-jargon-empowers padding-null d-flex justify-content-end align-items-end">
                    <p>
                      Empowers To Educate
                      </p>
                  </div>
                </MDBAnimation>
              </div>
            </div>
          </MDBView>

          <div className="row mt-5 " style={{ margin: '0.01%' }}>
            <div className='col-12 col-lg-10 d-flex justify-content-end align-items-end padding-null'>
              <h3 className="align-self-end">About <b>Us</b></h3>
            </div>
          </div>
          <div className="row" style={{ margin: '0.01%' }}>
            <div className='col-12 col-lg-2 d-flex justify-content-end padding-null' style={{ backgroundColor: '#fec71c' }}>
              <div className="my-about-div">
                <h3 className="d-none d-lg-block" title="zzzzzzzz" >EDWARD S. CURTIS CUK</h3>
              </div>
            </div>
            <div className='col-12 col-lg-3 padding-null'>
              <img
                className="img-fluid"
                src="https://blog.samys.com/wp-content/uploads/2017/03/curtisbio.jpg"
                alt="Third slide"
              />
            </div>
            <div className='col-12 col-lg-5 about-box'>
              <p className="" style={{ paddingTop: '8rem' }}>
                Quisquam aperiam, pariatur. Tempora, placeat ratione porro
                voluptate odit minima. Lorem ipsum dolor sit amet,
                consectetur adipisicing elit. Nihil odit magnam minima,
                soluta doloribus reiciendis molestiae placeat unde eos
                molestias.
              </p>
              <button className="align-self-end rounded-warning">View Our Profile</button>
            </div>
          </div>
          <MDBView>
            <div className="row mt-5 title-upcoming">
              <div className="col-1" />
              <div className="col-11 main-title">
                Upcoming
              </div>
            </div>
            <div className="row title-upcoming">
              <div className="col-1" style={{ backgroundColor: '#fec71c' }} />
              <div className="col-12 col-lg-2 sub-title">
                Events
              </div>
              <div className="col-12 col-lg-9 d-flex align-items-center">
                <button className="btn btn-outline-warning" style={{ fontSize: '1rem' }}>Selengkapnya</button>
              </div>
            </div>

            {/* List Events */}
            <MDBView>
              <div className="d-flex flex-row">
                <div className="d-none d-lg-block" style={{ width: '28vh' }}>
                </div>
                <div className="row grid-home-events">
                  <div onClick={() => this.linkTo('/sample')} className="col-12 col-lg-6 col-upcoming-events d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://gmedia.net.id/upload/foto_artikel/20200814NlwIohPBV4.png)" }}>
                    <div className="overlay-col-upcoming-events" style={{ backgroundColor: '#164d9f' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: 'snow' }}>Webinar series</h2>
                  </div>
                  <div className="col-12 col-lg-6 col-upcoming-events d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://homeschooling-sigmaphineta.com/wp-content/uploads/2019/09/artikel-2.jpg)" }}>
                    <div className="overlay-col-upcoming-events" style={{ backgroundColor: '#fec71c' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: '#164d9f' }}>Gelatik</h2>
                  </div>
                  <div className="col-12 col-lg-6 col-upcoming-events d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.elegantthemes.com/blog/wp-content/uploads/2020/10/divi-community-update-september-2020-scaled.jpg)" }}>
                    <div className="overlay-col-upcoming-events" style={{ backgroundColor: '#009a77' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: 'snow' }}>Community</h2>
                  </div>
                  <div className="col-12 col-lg-6 col-upcoming-events d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://edsource.org/wp-content/uploads/2020/05/Alliance-College-Ready-school-classroom.jpg)" }}>
                    <div className="overlay-col-upcoming-events" style={{ backgroundColor: '#164d9f' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: 'snow' }}>School Programs</h2>
                  </div>
                </div>
              </div>
            </MDBView>
          </MDBView>
          <MDBContainer fluid style={{ minHeight: '70vh', backgroundColor: '#fec71c', maxWidth: '100%' }}>
            <div className="row">
              <div className="col-12 col-lg-8 padding-null">
                <Slide easing="ease" {...properties} className="eachSlide">
                  {slideEvents.map((each, index) =>
                    <div className="each-slide">
                      <div className="content">
                        <img key={index} style={{ maxHeight: '25%', maxWidth: '65%' }} src={each.image} alt="" />
                        <br />
                        <p>{each.deskripsi}</p>
                        <a href={each.link} target="_blank" rel="noopener noreferrer"><button className="btn btn-outline-primary">Selengkapnya</button></a>
                      </div>
                    </div>
                  )
                  }
                </Slide>
              </div>
              <div className="col-4 padding-null">
                <div className="imgUE" style={{ backgroundImage: "url(https://wallpapercave.com/wp/wp4721470.jpg)" }}>
                </div>
              </div>
            </div>
          </MDBContainer>
          <MDBView style={{ minHeight: '70vh', maxWidth: '100%', marginTop: '10vh' }}>
            <div className="row title-upcoming">
              <div className="col-1">

              </div>
              <div className="col-11 main-title">
                Gallery
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-lg-1 padding-null">
              </div>
              <div className="col-12 col-lg-5 padding-null">
                <div className="row">
                  <div className="col-12 col-lg-6 col-gallery d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://gmedia.net.id/upload/foto_artikel/20200814NlwIohPBV4.png)" }}>
                    <div className="overlay-gallery" style={{ backgroundColor: '#164d9f' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: 'snow' }}>Webinar series</h2>
                  </div>
                  <div className="col-12 col-lg-6 col-gallery d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://homeschooling-sigmaphineta.com/wp-content/uploads/2019/09/artikel-2.jpg)" }}>
                    <div className="overlay-gallery" style={{ backgroundColor: '#fec71c' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: '#164d9f' }}>Gelatik</h2>
                  </div>
                  <div className="col-12 col-lg-6 col-gallery d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.elegantthemes.com/blog/wp-content/uploads/2020/10/divi-community-update-september-2020-scaled.jpg)" }}>
                    <div className="overlay-gallery" style={{ backgroundColor: '#009a77' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: 'snow' }}>Community</h2>
                  </div>
                  <div className="col-12 col-lg-6 col-gallery d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://edsource.org/wp-content/uploads/2020/05/Alliance-College-Ready-school-classroom.jpg)" }}>
                    <div className="overlay-gallery" style={{ backgroundColor: '#164d9f' }} />
                    <h2 className="title-content-events align-self-center" style={{ color: 'snow' }}>School Programs</h2>
                  </div>
                </div>
                <div className="d-flex justify-content-end align-items-end mt-3">
                  <button className="btn btn-warning">Whats On ?</button>
                </div>
              </div>
              <div className="col-12 col-lg-6 padding-null">
                <div className="row" >
                  <div className='col-12 col-lg-6 pr-0'>
                    <img
                      className="img-fluid"
                      src="https://blog.samys.com/wp-content/uploads/2017/03/curtisbio.jpg"
                      alt="Third slide"
                    />
                  </div>
                  <div className='col-12 col-lg-6 d-flex justify-content-start pr-0 pl-0' style={{ backgroundColor: '#fec71c' }}>
                    <div class="my-gallery-div">
                      <h3 className="d-none d-lg-block" title="zzzzzzzz" >EDWARD S. CURTIS CUK</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </MDBView>
        </div>
      </>
    );
  }
}

export default HomePage;
