import React from 'react';
import {
    MDBContainer,
    MDBBtn,
    MDBCol,
    MDBAnimation, MDBCardImage, MDBView, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter
} from 'mdbreact';
import { Modal } from 'react-bootstrap'
import { Link as LinkTo } from 'react-router-dom';
import '../../assets/css/Gallery.css';
import { Link, Element, Events } from 'react-scroll'
import { Slide } from 'react-slideshow-image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClone } from '@fortawesome/free-solid-svg-icons'

class MainGallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data_about: [],
            is_lang: [!localStorage.getItem('is_lang_now') ? 0 : JSON.parse(localStorage.getItem('is_lang_now')).param],
            options: [
                {
                    text: "Option 1",
                    value: "1"
                },
                {
                    text: "Option 2",
                    value: "2"
                },
                {
                    text: "Option 3",
                    value: "3"
                }
            ],
            modalDetailPhotos: false,
            pictureContent: true,
            videoContent: false,
            stateContent: 'photo',
            title: ''
        };
    }

    toggle(param) {
        this.setState({
            modalDetailPhotos: !this.state.modalDetailPhotos,
            title: param
        });
    }

    contentSwitch(param) {
        const { stateContent, pictureContent, videoContent } = this.state;
        if (param != stateContent) {
            this.setState({
                stateContent: param,
                pictureContent: !pictureContent,
                videoContent: !videoContent
            });
        }

    }

    scrollToTop = () => window.scrollTo(0, 0);

    async componentDidMount() {
        Events.scrollEvent.register('begin');
        Events.scrollEvent.register('end');
    }

    componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
    }

    render() {
        const { t } = this.props;
        const { title, modalDetailPhotos, pictureContent, videoContent } = this.state;
        const pictureCount = [{ title: 'Judul Gambar Satu' }, { title: 'Judul Gambar Dua' }, { title: 'Judul Gambar Tiga' }, { title: 'Judul Gambar Empat' }, { title: 'Judul Gambar Lima' }, { title: 'Judul Gambar Enam' }];
        const videoCount = [1, 2, 3, 4, 5, 6];
        const properties = {
            duration: 5000,
            transitionDuration: 500,
            infinite: true,
            indicator: true,
            prevArrow: <div style={{ width: "4vw", marginRight: "-30px", color: '#4e5d6c', fontSize: '3rem', cursor: 'pointer' }}><i className="fas fa-chevron-left ml-3"></i></div>,
            nextArrow: <div style={{ width: "4vw", marginLeft: "-30px", color: '#4e5d6c', fontSize: '3rem', cursor: 'pointer' }}><i className="fas fa-chevron-right"></i></div>,
            indicators: i => (<div className="indicator-custom"></div>)
        };
        const slideEvents = [
            {
                title: 'Judul Gambar Satu',
                image: 'https://www.kemenkumham.go.id/images/jux_portfolio_pro/pusdatin_new.png',
                deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
                link: 'https://www.kemenkumham.go.id/publikasi/logo'
            },
            {
                title: 'Judul Gambar Dua',
                image: 'https://assets.pikiran-rakyat.com/crop/0x393:3507x2707/x/photo/2020/11/24/4081988142.png',
                deskripsi: 'Pelaksanaan Upacara Bendera Peringatan Hari Guru Nasional 2020 tersebut dilaksanakan pada Rabu, 25 November 2020 mulai pukul 08:00 WIB, tidak hanya itu, dalam surat yang dikeluarkan Mendikbud tersebut juga menghimbau pelaksanaan upacara bendera harus menerapkan Protokol kesehatan Covid-19.',
                link: 'https://dialektikakuningan.pikiran-rakyat.com/pendidikan/pr-651009523/kemdikbud-imbau-masyarakat-meriahkan-hari-guru-nasional-2020-download-logo-resminya-di-sini'
            },
            {
                title: 'Judul Gambar Tiga',
                image: 'https://static.vecteezy.com/system/resources/previews/001/190/646/original/flower-watercolor-png.png',
                deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
                link: '#'
            },
            {
                title: 'Judul Gambar Empat',
                image: 'https://www.phiradio.net/wp-content/uploads/2020/02/logo_tikomdik.png',
                deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
                link: '#'
            },
            {
                title: 'Judul Gambar Lima',
                image: 'https://thumbs.dreamstime.com/b/aerial-view-lago-antorno-dolomites-lake-mountain-landscape-alps-peak-misurina-cortina-di-ampezzo-italy-reflected-103752677.jpg',
                deskripsi: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tempus nunc maximus consectetur. Curabitur fermentum tellus at tempor vehicula. Fusce et convallis elit, eget lobortis massa. Aliquam vel mauris libero. Sed malesuada dignissim turpis quis interdum. Proin sollicitudin, justo in sodales gravida, arcu nibh pellentesque ipsum, a ornare nibh elit nec erat. Quisque tincidunt mauris mi, vitae scelerisque quam rutrum eget. Duis ornare lectus molestie nulla auctor pellentesque.',
                link: '#'
            }
        ];
        return (
            <>
                <div className="header-image">
                    <header>
                        <div className="overlay"></div>
                        <img src="/assets/img/main_gallery.jpg" className="custom-img align-self-center d-none d-lg-block" alt="" />
                        <img src="/assets/img/phone_res.jpg" className="custom-img align-self-center d-block d-lg-none" alt="" />
                        <div className="contents">
                            <div className="row align-items-end" style={{ height: '50vh' }}>
                                <div className="col-12 d-flex title-header">
                                    Main Gallery
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
                <div style={{ minWidth: '98.4vw', position: 'absolute' }}>
                    <div className="row">
                        <div className="col-6 d-flex justify-content-center align-items-center p-2 title-nav-gallery" style={{ backgroundColor: '#FFAB2D' }} onClick={() => this.contentSwitch('photo')}>
                            Foto
                        </div>
                        <div className="col-6 d-flex justify-content-center align-items-center p-2 title-nav-gallery" style={{ backgroundColor: '#2A9AC4' }} onClick={() => this.contentSwitch('video')}>
                            Video
                        </div>
                    </div>
                </div>
                <div className="row" style={{ marginTop: '20vh', position: 'relative', marginRight: '0.01%' }}>
                    <div className="col-12 d-flex justify-content-end align-items-center pl-5">
                        Urutkan Berdasarkan
                        <div className="col-lg-3">
                            <div className="select">
                                <select>
                                    <option>Terbaru</option>
                                    <option>Terlama</option>
                                </select>
                                <div className="select_arrow">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <MDBView className="ml-4 mr-4">
                    <div className="row">
                        {
                            pictureContent && pictureCount.map((x, key) => {
                                return (
                                    <div className="col-sm-12 col-lg-4 padding-null pt-2 pb-2" key={key} onClick={() => this.toggle(x.title)}>
                                        <MDBAnimation type="fadeIn">
                                            <div className="d-flex justify-content-end align-items-end p-3" style={{ width: '95%', position: 'absolute'}}>
                                                { key % 2 == 0 ? <FontAwesomeIcon icon={faClone} size="3x" flip="horizontal" color="grey" /> : <></> }
                                            </div>
                                            <div style={{ position: 'relative' }}>
                                                <MDBCardImage className="img-fluid img-gallery-content" src="/assets/img/building.png" waves alt="" />
                                            </div>
                                        </MDBAnimation>
                                    </div>
                                )
                            })
                        }
                        {
                            videoContent && videoCount.map((x, key) => {
                                return (
                                    <div className="col-sm-12 col-lg-4 padding-null pt-2 pb-2" key={key}>
                                        <MDBAnimation type="fadeIn">
                                            <iframe className="embedded-video-16-9" height="100%" src="https://www.youtube.com/embed/pswdTJ59fdQ" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </MDBAnimation>
                                    </div>
                                )
                            })
                        }
                    </div>
                </MDBView>
                <MDBView style={{  marginRight: '0.01%' }}>
                    <div className="row title-upcoming" style={{ marginTop: '15vh' }}>
                        <div className="col-lg-1 col-sm-12" style={{ backgroundColor: '#fec71c' }} />
                        <div className="col-lg-2 col-sm-12 sub-title">
                            Lates News
                        </div>
                        <div className="col-lg-8 col-sm-12 d-flex align-items-center">
                            <button className="btn btn-outline-warning" style={{ fontSize: '1rem' }}>Selengkapnya</button>
                        </div>
                    </div>
                    <div className="row justify-content-md-center" style={{ margin: '0.01%' }}>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-yellow" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-blue" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                        <div className="col-12 col-lg-4 col-separator d-flex justify-content-center flex-column" style={{ backgroundImage: "url(https://www.goodtherapy.org/dbimages/ckupload_20171127170216_hands-together-teamwork.jpg)" }}>
                            <div className="overlay-col-separator-green" />
                            <h2 className="col-separator-subtitle align-self-center">"Everybody Get Up and Sleep Again"</h2>
                        </div>
                    </div>
                </MDBView>
                <MDBView>
                    <MDBModal
                        isOpen={modalDetailPhotos}
                        toggle={() => this.toggle()}
                        centered
                        size="xl"
                    >
                        <MDBModalHeader toggle={() => this.toggle('')}>{title}</MDBModalHeader>
                        <Slide easing="ease" {...properties} className="eachSlide">
                            {slideEvents.map((each, index) =>
                                <div className="each-slide-gallery" key={index}>
                                    <div >
                                        <MDBModalBody className="d-flex justify-content-center align-items-center">
                                            <img className="img-fluid" style={{ maxHeight: '25%', maxWidth: '65%', borderRadius: '5%' }} src={each.image} alt="" />
                                        </MDBModalBody>
                                        <MDBModalFooter>
                                            <p className="content">{each.deskripsi}</p>
                                        </MDBModalFooter>
                                    </div>
                                </div>
                            )}
                        </Slide>
                    </MDBModal>
                </MDBView>
            </>
        );
    }
};
export default MainGallery;
