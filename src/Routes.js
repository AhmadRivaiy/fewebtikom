import React from 'react';
import { Route, Switch } from 'react-router-dom';

//Root
import HomePage from './pages/home/HomePage';

//Main Menu
import MainGallery from './pages/gallery/MainGallery';
import MainPrograms from './pages/programs/MainPrograms';

//Detail Programs
import DetailPrograms from './pages/programs/DetailPrograms';

//Our Partner
import Partners from './pages/our_partner/Partners';

//News Main
import NewsPage from './pages/news/NewsPage';

//News Detail
import NewsPageEducation from './pages/news/NewsPageEducation';
import NewsPageTecnology from './pages/news/NewsPageTecnology';
import NewsPageSystem from './pages/news/NewsPageSystem';
import DetailNews from './pages/news/NewsDetail';

//Events Page
import EventsPage from './pages/events/MainEvents';

//NotFound
import NotFoundPage from './pages/template/NotFound'

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/main-gallery' component={MainGallery} />

        //Programs
        <Route exact path='/main-programs' component={MainPrograms} />
        <Route path='/main-programs/:tag' component={DetailPrograms} />

        //Our Partner
        <Route exact path='/partners' component={Partners} />
        
        //News
        <Route exact path='/news' component={NewsPage} />
        <Route exact path='/news/education' component={NewsPageEducation} />
        <Route exact path='/news/technology' component={NewsPageTecnology} />
        <Route exact path='/news/system' component={NewsPageSystem} />
        <Route exact path='/news/:base/detail/:tag' component={DetailNews} />

        //Events
        <Route exact path='/events' component={EventsPage} />

        <Route
          // render={function() {
          //   return <div className="d-flex justify-content-center align-items-center" style={{ height: '50vh' }}> <h1 className="text-center">404 Not Found</h1></div>;
          // }}
          component={NotFoundPage}
        />
      </Switch>
    );
  }
}

export default Routes;
