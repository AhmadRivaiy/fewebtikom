import React, { Component } from 'react';
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBCollapse,
  MDBNavItem,
  MDBFooter,
  MDBNavLink,
  MDBTooltip,
  MDBIcon, MDBView,
  MDBRow, MDBCol, MDBContainer
} from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';
import logoTikom from './assets/logo_tikomdik.png';
import logoDisdik from './assets/disdikjabar.png';
import Routes from './Routes';

import { detect } from 'detect-browser';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { userService } from './services';

class App extends Component {
  state = {
    collapseID: '',
    activeID: '',
    sidebarOpen: false
  };

   async componentDidMount(){
    await axios.get('https://api64.ipify.org?format=json').then(x => {
      const data = {
        ip_user: x.data.ip,
        browser_name: detect().name,
        browser_version: detect().version,
        user_os: detect().os
      }
      userService.postData('http://localhost:1998/visited_web', data).then(
        user =>{
          console.log(user);
        }
      )
    })
   }

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));

  closeCollapse = collID => () => {
    const { collapseID } = this.state;
    window.scrollTo(0, 0);
    collapseID === collID && this.setState({ collapseID: '' });
  };

  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }
  
  render() {
    const overlay = (
      <div
        id='sidenav-overlay'
        style={{ backgroundColor: 'transparent' }}
        onClick={this.toggleCollapse('mainNavbarCollapse')}
      />
    );

    const { collapseID, sidebarOpen, activeID } = this.state;
      console.log(activeID)
    return (
      <Router>
            <div className='flyout'>
          {/* <Sidebar
            sidebar={<b>Sidebar content</b>}
            open={sidebarOpen}
            onSetOpen={() => this.onSetSidebarOpen(!sidebarOpen)}
            touch={true}
            styles={{ sidebar: { background: "white" } }}
          > */}
              <MDBNavbar color='white' light expand='md' fixed='top' scrolling>
                <MDBNavbarToggler
                  onClick={this.toggleCollapse('mainNavbarCollapse')}
                  //onClick={() => this.setState({ sidebarOpen: !sidebarOpen })}
                  right={true}
                  image="https://www.iconfinder.com/data/icons/basic-ui-27/64/Artboard_18-512.png"
                />
                <MDBNavbarBrand href='/' className='py-0 d-flex justify-content-center' >
                  <img src={logoTikom} style={{ height: 'auto', width: '8.5rem', alignItems: 'center', marginLeft: '8rem' }} alt="Logo UPTD Tikomdik" />
                  {/* <h6 className="text-center" style={{ alignItems: 'center', margin: '0.1em' }}>UPTD TIKomDik<br/>Jawa Barat</h6> */}
                </MDBNavbarBrand>
                <MDBCollapse id='mainNavbarCollapse' isOpen={collapseID} navbar>
                  <MDBNavbarNav right>
                    <MDBNavItem className="d-block d-md-none">
                      <MDBView
                        disabled={true}
                      >
                        <br />
                      </MDBView>
                    </MDBNavItem>
                    <MDBNavItem active={false}>
                      <MDBNavLink
                        exact
                        to='/'
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        className="titleNav activeNav"
                      >
                        Home
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/news'
                        className="titleNav"
                      >
                        News
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/main-programs'
                        className="titleNav"
                      >
                        Programs
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/events'
                        className="titleNav"
                      >
                        Events
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/main-gallery'
                        className="titleNav"
                      >
                        Gallery
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/tables'
                      >
                        <strong>About</strong>
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/partners'
                        className="titleNav"
                      >
                        Our Partner
                      </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink
                        onClick={this.closeCollapse('mainNavbarCollapse')}
                        to='/addons'
                        className="titleNav"
                      >
                        <FontAwesomeIcon icon={faUser} style={{ color: 'dark' }} />
                      </MDBNavLink>
                    </MDBNavItem>
                    {/* <MDBNavItem>
                      <MDBTooltip
                        placement='bottom'
                        domElement
                        style={{ display: 'block' }}
                      >
                        <a
                          className='nav-link Ripple-parent'
                          href='https://mdbootstrap.com/products/react-ui-kit/'
                          target='_blank'
                          rel='noopener noreferrer'
                        >
                          <strong>
                            <MDBIcon far icon='gem' />
                          </strong>
                        </a>
                        <span>PRO</span>
                      </MDBTooltip>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBTooltip
                        placement='bottom'
                        domElement
                        style={{ display: 'block' }}
                      >
                        <a
                          className='nav-link Ripple-parent'
                          href='https://mdbootstrap.com/docs/react/getting-started/download/'
                          target='_blank'
                          rel='noopener noreferrer'
                        >
                          <strong>
                            <MDBIcon icon='download' />
                          </strong>
                        </a>
                        <span>FREE</span>
                      </MDBTooltip>
                    </MDBNavItem>
                    <MDBNavItem className='mr-2'>
                      <MDBTooltip
                        placement='bottom'
                        domElement
                        style={{ display: 'block' }}
                      >
                        <a
                          className='nav-link Ripple-parent'
                          href='https://mdbootstrap.com/support/cat/react/'
                          target='_blank'
                          rel='noopener noreferrer'
                        >
                          <strong>
                            <MDBIcon icon='question-circle' />
                          </strong>
                        </a>
                        <span>SUPPORT</span>
                      </MDBTooltip>
                    </MDBNavItem> */}
                  </MDBNavbarNav>
                </MDBCollapse>
              </MDBNavbar>
              {collapseID && overlay}
              <main style={{ marginTop: '4rem' }}>
                <Routes />
              </main>
              <MDBFooter className="pt-4" style={{ backgroundColor: '#0f4a96' }}>
                <MDBContainer fluid className='text-left' style={{ fontSize: 12 }}>
                  <MDBRow style={{display: 'flex', justifyContent: 'center', alignItems: 'center', padding: '0.8rem', flexDirection: 'row', margin: 15}} >
                    <MDBCol lg='2' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
                      <div className="p-3">
                        <img src={logoDisdik} style={{ height: 'auto', width: '150px' }} alt="Logo UPTD Tikomdik" />
                      </div>
                      <div>
                        <img src={logoTikom} style={{ height: 'auto', width: '150px' }} alt="Logo UPTD Tikomdik" /> 
                      </div>
                    </MDBCol>
                    <MDBCol lg='2'>
                        <MDBView className="d-flex flex-column">
                          <h5 className='titleFooter'>Product</h5>
                          <ul className='list-unstyled listContentFooter'>
                            <li>
                              <a href='#!'>Features</a>
                            </li>
                            <li>
                              <a href='#!'>Tutorials</a>
                            </li>
                            <li>
                              <a href='#!'>Templates</a>
                            </li>
                            <li>
                              <a href='#!'>Try it Free</a>
                            </li>
                          </ul>
                        </MDBView>
                    </MDBCol>
                    <MDBCol lg='2'>
                        <MDBView className="d-flex flex-column">
                          <h5 className='titleFooter'>Support</h5>
                          <ul className='list-unstyled listContentFooter'>
                            <li>
                              <a href='#!'>Forum Atikan</a>
                            </li>
                            <li>
                              <a href='#!'>Contact Support</a>
                            </li>
                            <li>
                              <a href='#!'>Knowledge Hub</a>
                            </li>
                            <li>
                              <a href='#!'>Download</a>
                            </li>
                          </ul>
                        </MDBView>
                    </MDBCol>
                    <MDBCol lg='2'>
                      <MDBView className="d-flex flex-column">
                        <h5 className='titleFooter'>About Us</h5>
                        <ul className='list-unstyled listContentFooter'>
                          <li>
                            <a href='#!'>About Us</a>
                          </li>
                          <li>
                            <a href='#!'>Tikompedia</a>
                          </li>
                          <li>
                            <a href='#!'>Press Kit</a>
                          </li>
                          <li>
                            <a href='#!'>Term of Service</a>
                          </li>
                        </ul>
                      </MDBView>
                    </MDBCol>
                    <MDBCol lg='2'>
                      <MDBView className="d-flex flex-column">
                        <h5 className='titleFooter'>Handy Links</h5>
                        <ul className='list-unstyled listContentFooter'>
                          <li>
                            <a href='#!'>Pemdaprov Jabar</a>
                          </li>
                          <li>
                            <a href='#!'>Disdik Jabar</a>
                          </li>
                          <li>
                            <a href='#!'>Humas Jabar</a>
                          </li>
                          <li>
                            <a href='#!'>Other Services</a>
                          </li>
                        </ul>
                      </MDBView>
                    </MDBCol>
                    <MDBCol lg='2'>
                      <MDBView className="d-flex flex-column">
                        <h5 className='titleFooter'>Connect With Us</h5>
                        <ul className='list-unstyled listContentFooter' style={{ display: 'flex', justifyContent: 'center', alignItems: 'flex-start', flexDirection: 'column'}}>
                          <li>
                            <a href='#!' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}><span><i className="fab fa-facebook-square fa-2x mr-2"></i></span>tikomdik jabar</a>
                          </li>
                          <li>
                            <a href='#!' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}><span><i className="fab fa-instagram-square fa-2x mr-2"></i></span>tikomdik_jabar</a>
                          </li>
                          <li>
                            <a href='#!' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}><span><i className="fab fa-youtube-square fa-2x mr-2"></i></span>Tikomdik Disdik Jabar</a>
                          </li>
                        </ul>
                      </MDBView>
                    </MDBCol>
                    <MDBCol size='12'>
                      <hr className="hrWhite"></hr>
                    </MDBCol>
                    <MDBCol size='12' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', padding: 15}}>
                      <div className="footerCopy">©2020 UPTD Tikomdik Disdik Jabar. All Rights Reserved.</div>
                    </MDBCol>
                  </MDBRow>
                </MDBContainer>
              </MDBFooter>
        {/* </Sidebar> */}
            </div>
      </Router>
    );
  }
}

export default App;
