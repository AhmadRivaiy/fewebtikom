module.exports = {
  apps : [{
    name:'web-tikom-fe',
    script: './src/index.js',
    watch: '.',
    //ignore_watch: ['api/logapi','tmp'],
    // env: {
    //   "PORT": "6855",
    //   "SSO":"https://api.atikan.tikomdik-disdikjabar.id/sso",
    //   "URL_LOGIN_LOKAL" : "http://192.168.2.140:6200",
    //   "URL_DB": "postgres://postgre:Tikomdikdb2020@192.168.100.87:5432/SyncDapo2020",
    //   "PPDB_URL":"http://202.93.229.134/webservice",
    //   "PPDB_CONF_KEY":"b97c03f7c2940bc0e88b794c53f86426",
    //   "JWT_CONF_TOKEN": "82afwqfqw3twqt9b446054339439ac73f350e533e78ea48a582bf115253e5675",
    //   "NODE_ENV":"development"
    // }
  }],

  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
